/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <stdio.h>
#include <stdlib.h>

#include <ros/ros.h>

#include "ca_common/coordconv.h"
#include "ca_common/math.h"

using namespace CA;
using namespace CA::math_consts;
using namespace std;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "example_coordconv");
  ros::NodeHandle n;

  //Create conversion class
  fprintf(stderr, "Creating CoordinateConversion()...\n");
  CoordinateConversion *coordConv = new CoordinateConversion();

  int res;
  double lat, lon;
  double utmN, utmE;
  utmN = 4276400.00;
  utmE =  282400.00;

  //Configure UTM zone and hemisphere
  /*
  res = coordConv->setUTMZone(18, 'N');
  if (res != 0) {
    fprintf(stderr, "Failed to set UTM zone - error code = %d\n", res);
    exit(1);
  }
  fprintf(stderr, "\n");
  */
  
  //Conversion from UTM to Lat/Long
  fprintf(stderr, "Input UTM: North=%f East=%f\n", utmN, utmE);
  res = coordConv->UTM2LatLongRad(utmN, utmE, &lat, &lon);
  if (res != 0) {
    fprintf(stderr, "Conversion failed - error code = %d\n", res);
  }
  else {
    fprintf(stderr, "Output[%d]: Lat=%f Lon=%f\n", res, lat * RAD2DEG, lon * RAD2DEG);
  }

  fprintf(stderr, "\n");

  //Conversion from Lat/Long to UTM
  fprintf(stderr, "Input: Lat=%f Lon=%f\n", lat * RAD2DEG, lon * RAD2DEG);
  res = coordConv->LatLongRad2UTM(lat, lon, &utmN, &utmE);
  if (res != 0) {
    fprintf(stderr, "Conversion failed - error code = %d\n", res);
  }
  else {
    fprintf(stderr, "Output UTM[%d]: North=%f East=%f\n", res, utmN, utmE);
  }

  fprintf(stderr, "\n");

  //Conversion from Lat/Long to UTM
  res = coordConv->LatLongDeg2UTM(38.0, 36.0, 34.00, -77.0, -29.0, -56.7, &utmN, &utmE);
  if (res != 0) {
    fprintf(stderr, "Conversion failed - error code = %d\n", res);
  }
  else {
    fprintf(stderr, "Output UTM[%d]: North=%f East=%f\n", res, utmN, utmE);
  }

  fprintf(stderr, "\n");

  //Release conversion class
  fprintf(stderr, "Releasing CoordinateConversion()...\n");
  delete coordConv;

  fprintf(stderr, "Finishing test.\n");

  return 0;
}
