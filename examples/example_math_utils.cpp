/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ca_common/math_utils.h"

using namespace CA;

// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  std::cout<<"\n Wrapping -1 from [0 10] equals: "<<math_utils::numeric_operations::Wrap(-1, 0, 10)<<"\n";
}




