/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2014 Sanjiban Choudhury
 * math_utils_test.cpp
 *
 *  Created on: Jan 1, 2014
 *      Author: sanjiban
 */

#include "ca_common/math.h"
#include <iostream>
// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  CA::State state;
  state.pose.position_m = CA::Vector3D(10,11,12);
  std::cout << "\n Length is " << CA::math_tools::Length(state.pose.position_m) <<" \n";
  std::cout << "But eigen already has this!!! Eigen says length is " << state.pose.position_m.norm() << "\n";
  std::cout << "limit to pi is "<<CA::math_tools::limit2pi(5)<<"\n";
}

