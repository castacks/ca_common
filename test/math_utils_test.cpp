/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2014 Sanjiban Choudhury
 * math_utils_test.cpp
 *
 *  Created on: Jan 1, 2014
 *      Author: sanjiban
 */

// Bring in gtest
#include <gtest/gtest.h>
#include "ca_common/math_utils.h"


using namespace CA;
std::string filename;

TEST(VectorMath, intersectionTest1) {
  Vector3D intersection;
  ASSERT_TRUE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(10,10,0), Vector3D(20,10,0), Vector3D(30, 0, 0), intersection))<<"Failed to find intersection";
  EXPECT_FLOAT_EQ(15, intersection.x()) << "X value is wrong";
  EXPECT_FLOAT_EQ(15, intersection.y()) << "Y value is wrong";
}

TEST(VectorMath, intersectionTest2) {
  Vector3D intersection;
  ASSERT_TRUE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(10,10,0), Vector3D(10,10,0), Vector3D(30, 30, 0), intersection))<<"Failed to find intersection";
  EXPECT_FLOAT_EQ(10, intersection.x()) << "X value is wrong";
  EXPECT_FLOAT_EQ(10, intersection.y()) << "Y value is wrong";
}

TEST(VectorMath, intersectionTest3) {
  Vector3D intersection;
  ASSERT_FALSE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(10,10,0), Vector3D(-10,10,0), Vector3D(0, 0, 0), intersection))<<"Failed to find intersection";
}

TEST(VectorMath, intersectionTest4) {
  Vector3D intersection;
  ASSERT_FALSE(math_utils::vector_math::Intersection2D(Vector3D(10,0,0), Vector3D(10,10,0), Vector3D(20,10,0), Vector3D(20, 0, 0), intersection))<<"Failed to find intersection";
}

TEST(VectorMath, intersectionTest5) {
  Vector3D intersection;
  ASSERT_FALSE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(10,10,0), Vector3D(20,10,0), Vector3D(30, 20, 0), intersection))<<"Failed to find intersection";
}

TEST(VectorMath, intersectionTest6) {
  Vector3D intersection;
  ASSERT_TRUE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(10,10,0), Vector3D(20,10,0), Vector3D(30, 10, 0), intersection))<<"Failed to find intersection";
  EXPECT_FLOAT_EQ(10, intersection.x()) << "X value is wrong";
  EXPECT_FLOAT_EQ(10, intersection.y()) << "Y value is wrong";
}

TEST(VectorMath, intersectionTest7) {
  Vector3D intersection;
  ASSERT_TRUE(math_utils::vector_math::Intersection2D(Vector3D(0,0,0), Vector3D(1,10,0), Vector3D(11,10,0), Vector3D(12, 0, 0), 2, intersection))<<"Failed to find intersection";
  EXPECT_FLOAT_EQ(6, intersection.x()) << "X value is wrong";
  EXPECT_FLOAT_EQ(12, intersection.y()) << "Y value is wrong";
}

TEST(NumericOperations, wrapTest1) {
  EXPECT_EQ(10, math_utils::numeric_operations::Wrap(-1, 0, 10));
}

TEST(NumericOperations, wrapTest2) {
  EXPECT_EQ(0, math_utils::numeric_operations::Wrap(11, 0, 10));
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}




