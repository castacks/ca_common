/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/**\file unittests.cpp
  * \brief   Defines the unit tests for the library
  * \author  Sebastian Scherer <basti@cmu.edu>, Andrew Chambers <achamber@andrew.cmu.edu>
  * Set up the unit tests to exercise the functionality of the library
  */

#include <gtest/gtest.h>
#include <stdlib.h>
//#include <mm/common/commontypes.h>
#include <nav_msgs/Odometry.h>
#include <ca_common/math.h>

using namespace CA;

TEST(parentVelocity_test, test_case_1)
{
	nav_msgs::Odometry odomMsg;
	odomMsg.pose.pose.orientation.x = 0.5;
	odomMsg.pose.pose.orientation.y = 0.5;
	odomMsg.pose.pose.orientation.z = 0.5;
	odomMsg.pose.pose.orientation.w = 0.5;

	odomMsg.twist.twist.linear.x = 1;
	odomMsg.twist.twist.linear.y = 2;
	odomMsg.twist.twist.linear.z = 3;

	Vector3D pVel = math_tools::parentVelocity(odomMsg);

	Vector3D expectedResult(3, 1, 2);

  ASSERT_TRUE((pVel.array() == expectedResult.array()).all());
}

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
