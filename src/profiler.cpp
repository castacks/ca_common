/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ca_common/profiler.h>

#include <string>
#include <ca_common/ProfilerMsg.h>

using namespace std;
using namespace CA;

std::map<std::string, CA::ProfilerItem > CA::Profiler::m_times;

bool CA::Profiler::m_init = false;

ros::Publisher  CA::Profiler::m_profilemarker;

void Profiler::start(const std::string &label)
{

    m_times[label].starttime        = ros::WallTime::now().toSec();
    m_times[label].sectionstarttime = m_times[label].starttime;
    m_times[label].cummulativetime  = 0.0;
    m_times[label].maxtime = 0.0;
}

void Profiler::init(const std::string &label)
{

    m_times[label].starttime        = ros::WallTime::now().toSec();
    m_times[label].sectionstarttime = -1;
    m_times[label].cummulativetime  = 0.0;
    m_times[label].maxtime = 0.0;
}

void Profiler::sectionStart(const std::string &label)
{
    map<string,ProfilerItem>::iterator iter = m_times.find(label);
    if(iter!= m_times.end())
    {
        (*iter).second.sectionstarttime =  ros::WallTime::now().toSec();
    } else
    {
        ROS_WARN_STREAM("Not recording section start. Label not found: "<<label);
    }
}

void Profiler::sectionStop(const std::string &label)
{
    double currtime = ros::WallTime::now().toSec();
    map<string,ProfilerItem>::iterator iter = m_times.find(label);
    if(iter != m_times.end() && (*iter).second.sectionstarttime!=-1 )
    {
        double period = currtime - (*iter).second.sectionstarttime;
        (*iter).second.cummulativetime += period;
        (*iter).second.sectionstarttime = -1;
        if(period > (*iter).second.maxtime)
            (*iter).second.maxtime = period;
    } else
    {
        ROS_WARN_STREAM("Not recording section stop for label. Label not found "<<label);
    }
}

double Profiler::stop(const std::string &label,const std::vector<double> &otherdata)
{

    double td = -1.0;
    double currtime = ros::WallTime::now().toSec();
    map<string,ProfilerItem>::iterator iter = m_times.find(label);
    if(iter != m_times.end()  )
    {
        double lastdt = 0.0;
        if((*iter).second.sectionstarttime>0)
            lastdt =  currtime - (*iter).second.sectionstarttime;
        td=  (*iter).second.cummulativetime +lastdt;
        (*iter).second.cummulativetime = 0.0;

        ca_common::ProfilerMsg profilem;
        static unsigned int i=0;
        i++;
        profilem.seq      = i;
        profilem.starttime       = (*iter).second.starttime;
        profilem.endtime         = currtime;
        profilem.label           = label;
        profilem.deltatime_s     = td;
        profilem.otherdata       = otherdata;
        if( ros::isInitialized() )
        {
            if(!m_init )
            {
                ros::NodeHandle n;
                m_profilemarker = n.advertise<ca_common::ProfilerMsg>("/profiler",20);
                m_init = true;
            }
            m_profilemarker.publish(profilem);

        }
    } else
    {
        ROS_WARN_STREAM("In Stop: Profiler label not found: "<<label<<". Skipping profiling.");
    }
    return td;
}

double Profiler::max(const std::string &label)
{
    map<string,ProfilerItem>::iterator iter = m_times.find(label);
    if(iter != m_times.end())
    {
        return((*iter).second.maxtime);
    } else
    {
        ROS_WARN_STREAM("Label not found "<<label);
        return -1;
    }
}
