/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * ros_wrapper.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: sanjiban
 */

#include "ca_common/ros_wrapper.h"
using namespace CA;

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, std::string& s) {
    bool result = n.getParam(str, s);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, double& d) {
    bool result = n.getParam(str, d);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, float& d) {
    double d1;
    bool result = n.getParam(str, d1);
    d = static_cast<float>(d1);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, int& i) {
    bool result = n.getParam(str, i);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

bool ROS::getParam(ros::NodeHandle &n, const std::string& key, unsigned int & i) {
    int i2;
    bool result = n.getParam(key, i2);
    i = static_cast<unsigned int>(i2);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<key);
    return result;
}
bool ROS::getParam(ros::NodeHandle &n, const std::string& key, unsigned char& i) {
    int i2;
    bool result = n.getParam(key, i2);
    i = static_cast<unsigned char>(i2);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<key);
    return result;
}

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, bool& b) {
    bool result = n.getParam(str, b);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

bool ROS::getParam (ros::NodeHandle &n, const std::string &str, XmlRpc::XmlRpcValue& v) {
    bool result = n.getParam(str, v);
    if (!result)
        ROS_ERROR_STREAM("Did not get param: "<<str);
    return result;
}

namespace ROS {
namespace Visualization {
bool GetMarker(const Trajectory &traj, visualization_msgs::Marker &marker) {
    marker.action = visualization_msgs::Marker::ADD;
    marker.scale = msgc(Vector3D(1,1,1));
    marker.pose.orientation = msgc(ZEROROT_D);
    marker.pose.position = msgcp(ZERO_3D);
    marker.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
    marker.color.r = 0.0;
    marker.color.g = 0.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t = traj.getLinear();
    geometry_msgs::Point ps;
    BOOST_FOREACH(State &sc,t) {
        ps = msgcp(sc.pose.position_m);
        marker.points.push_back(ps);
    }
    return true;
}

}
}
