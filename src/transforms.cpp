/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * transforms.cpp
 *
 *  Created on: Jul 8, 2013
 *      Author: achamber
 */

#include <ca_common/transforms.h>

using namespace CA;
using namespace std;

bool CA::findTransformation(const tf::TransformListener* tf_listener, const std::string& src,
                            const std::string& dst, const ros::Time stamp,
                            const ros::Duration duration, Transform3D& result)
{
    Transform3D* transform = findTransformation(tf_listener, src, dst, stamp, duration);

    if (transform) {
        result = *transform;
        delete transform;
        return true;
    } else {
        return false;
    }
}

Transform3D* CA::findTransformation(
    const tf::TransformListener* tf_listener, const std::string& src,
    const std::string& dst, const ros::Time stamp,
    const ros::Duration duration) {
    // Lookup the transform from src to dst frame

    Transform3D* T_src_dst(0);

    bool validTF = tf_listener->waitForTransform(src, dst, stamp, duration );
    if (validTF) {
        tf::StampedTransform stampedtf;
        tf_listener->lookupTransform(src , dst , stamp, stampedtf);

        // Convert the TF transform to an Eigen Isometry transform
        Eigen::Affine3d af;
#if ROS_VERSION_MINIMUM(1, 9, 50)
        // >= groovy
        tf::transformTFToEigen(stampedtf, af);
#else
        tf::TransformTFToEigen(stampedtf, af);
#endif

        T_src_dst = new Eigen::Isometry3d();
        T_src_dst->matrix() = af.matrix();
    } else {
        ROS_ERROR_STREAM("Cannot find transform from " << src << " to " << dst << ".");
    }

    return T_src_dst;
}

bool TFUtils::getCoordinateTransform(tf::StampedTransform &transform, tf::TransformListener &listener,std::string fromFrame, std::string toFrame, double waitTime)
{
    bool flag = true;
    double timeResolution = 0.001;
    unsigned int timeout = ceil(waitTime/timeResolution);
    unsigned int count = 0;

    while(flag)
    {
        flag = false;
        try
        {
            count++;
            listener.lookupTransform(toFrame,fromFrame,ros::Time(0), transform);
        }
        catch (tf::TransformException ex)
        {
            flag = true;
            if(count>timeout)
            {
                flag = false;
                ROS_ERROR_STREAM("Cannot find transform from::"<<fromFrame << " to::"<< toFrame);
                return flag;
            }
            ros::Duration(timeResolution).sleep();
        }
    }

    return (!flag);

};

void TFUtils::transformQuaternion(tf::Transform &transform,geometry_msgs::Quaternion& quat_in)
{
    tf::Quaternion q,qOut;
    tf::quaternionMsgToTF(quat_in, q);
    qOut = transform*q;
    tf::quaternionTFToMsg(qOut,quat_in);
};

void TFUtils::transformPose(tf::Transform &transform,geometry_msgs::Pose& pose_in)
{
    tf::Pose p,pOut;
    tf::poseMsgToTF(pose_in, p);
    pOut = transform*p;
    tf::poseTFToMsg(pOut,pose_in);
};

void TFUtils::transformPoint(tf::Transform &transform,geometry_msgs::Point& point_in)
{
    tf::Point p,pOut;
    tf::pointMsgToTF(point_in, p);
    pOut = transform*p;
    tf::pointTFToMsg(pOut,point_in);
};

void TFUtils::transformPointList(tf::Transform &transform,std::vector<geometry_msgs::Point>& point_in)
{
    for(size_t i=0; i< point_in.size(); i++)
    {
        transformPoint(transform,point_in[i]);
    }
};


void TFUtils::transformVector(tf::Transform &transform,geometry_msgs::Vector3& vector_in)
{
    tf::Vector3 v,vOut;
    tf::vector3MsgToTF(vector_in, v);
    vOut = transform*v;
    tf::vector3TFToMsg(vOut,vector_in);
};

void TFUtils::transformMarker(tf::Transform &transform, visualization_msgs::Marker &marker)
{
    transformPose(transform,marker.pose);
    for(size_t i=0; i<marker.points.size(); i++)
    {
        transformPoint(transform,marker.points[i]);
    }
};

void TFUtils::transformTrajectory(tf::Transform &transform, ca_common::Trajectory &trajectory)
{
    for(size_t i=0; i<trajectory.trajectory.size(); i++)
    {
        Vector3D rpy(0.0,0.0,trajectory.trajectory[i].heading);
        geometry_msgs::Quaternion q = CA::msgc(CA::eulerToQuat(rpy));
        transformQuaternion(transform,q);
        trajectory.trajectory[i].heading = CA::quatToEuler(msgc(q)).z();

        transformPoint(transform, trajectory.trajectory[i].position);
    }
}

void TFUtils::transformTrajectory(tf::Transform &transform, Trajectory &trajectory)
{
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t = trajectory.getLinear();
    for(size_t i = 0; i < t.size(); i++)
    {
        transformVector3D(transform, t[i].pose.position_m);
        geometry_msgs::Quaternion q = msgc(eulerToQuat(t[i].pose.orientation_rad));
        transformQuaternion(transform, q);
        t[i].pose.orientation_rad = quatToEuler(msgc(q));
    }
    trajectory.setLinear(t);
}

void TFUtils::transformOdom(tf::Transform &transform, nav_msgs::Odometry &odom)
{
    TFUtils::transformPose(transform, odom.pose.pose);
    //ROS_WARN("This transformOdom function only transforms pose");
}

void TFUtils::transformState(tf::Transform &transform, CA::State &state) {
    transformVector3D(transform, state.pose.position_m);
    geometry_msgs::Quaternion q = msgc(eulerToQuat(state.pose.orientation_rad));
    transformQuaternion(transform, q);
    state.pose.orientation_rad = quatToEuler(msgc(q));
}

void TFUtils::transformOdom(tf::Transform &transformPose, tf::Transform &transformVelocity, nav_msgs::Odometry &odom)
{
    TFUtils::transformPose(transformPose, odom.pose.pose);
    tf::Vector3 v(0,0,0);
    transformVelocity.setOrigin(v);
    TFUtils::transformVector(transformVelocity, odom.twist.twist.linear);
}

void TFUtils::transformVector3D(tf::Transform &transform, Vector3D &vec)
{
    geometry_msgs::Point pt = msgcp(vec);
    TFUtils::transformPoint(transform, pt);
    vec = msgc(pt);
}
