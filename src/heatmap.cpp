/*
* Copyright (c) 2016 Carnegie Mellon University, Author <ar.sankalp@gmail.com>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ca_common/heatmap.h>

using namespace CA;

void HeatMap::addColorPoint( std_msgs::ColorRGBA &color,float &value)
{
    for(unsigned int i=0; i<_colors.size(); i++)
    {
        if( value < _colors[i]._value )
        {
            _colors.insert( _colors.begin()+i,ColorPoint(color,value) );
            return;
        }
    }
    _colors.push_back( ColorPoint(color,value) );
};


void HeatMap::create5ColortHeatMap()
{
    _colors.clear();
    _colors.push_back( ColorPoint(0.0, 0.0, 1.0,1.0,   0.0f ) );      // blue
    _colors.push_back( ColorPoint(0.0, 1.0, 1.0,1.0,   0.25f) );      // cyan
    _colors.push_back( ColorPoint(0.0, 1.0, 0.0,1.0,   0.5f ) );      // green
    _colors.push_back( ColorPoint(1.0, 1.0, 0.0,1.0,   0.75f) );      // yellow
    _colors.push_back( ColorPoint(1.0, 0.0, 0.0,1.0,   1.0f ) );      // red
};

void HeatMap::create3ColortHeatMap()
{
    _colors.clear();
    _colors.push_back( ColorPoint(0.0, 1.0, 0.0,1.0,   0.0f ) );      // green
    _colors.push_back( ColorPoint(1.0, 1.0, 0.0,1.0,   0.5f) );      // yellow
    _colors.push_back( ColorPoint(1.0, 0.0, 0.0,1.0,   1.0f ) );      // red
};

std_msgs::ColorRGBA HeatMap::getHeatMapColor(const float &min,const float &max, float value)
{
    std_msgs::ColorRGBA color;
    color.a =1.0;
    if(max<=min || _colors.size() == 0)
    {
        color.r=0.0;
        color.g=0.0;
        color.b=1.0;
        return color;

    }
    else if(_colors.size() == 1)
    {
        return _colors[0]._color;
    }
    else
    {
        float normalization_factor = max-min;
        value  = (value - min)/normalization_factor;
        unsigned int i;
        for(i=0; i<_colors.size(); i++)
        {
            if( value < _colors[i]._value )
            {
                if(i==0)
                    color = _colors[i]._color;
                else
                {
                    value = (value - _colors[i-1]._value)/(_colors[i]._value - _colors[i-1]._value);
                    float temp = 1.0- value;
                    color.r = _colors[i]._color.r*value + _colors[i-1]._color.r*temp;
                    color.g = _colors[i]._color.g*value + _colors[i-1]._color.g*temp;
                    color.b = _colors[i]._color.b*value + _colors[i-1]._color.b*temp;
                    color.a = _colors[i]._color.a*value + _colors[i-1]._color.a*temp;
                    return color;
                }
            }
        }

        return _colors[i-1]._color;
    }
};



