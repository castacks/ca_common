/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ca_common/shapes.h"

using namespace CA;

void ShapeVector::clearShapeVector()
{
    Shape* sh;
    while(shapeVector.size()>0)
    {
        sh = shapeVector.back();
        delete sh;
        shapeVector.pop_back();
    };
};

visualization_msgs::MarkerArray ShapeVector::returnShapeMarkerArray()
{
    visualization_msgs::MarkerArray marker_array;
    for (unsigned int i=0; i<shapeVector.size(); i++)
    {
        marker_array.markers.push_back(shapeVector[i]->returnShapeMarker(i));
    }

    return marker_array;
};

ShapeVector::~ShapeVector()
{
    clearShapeVector();
};


Cylinder::Cylinder(Vector3D &p1_,Vector3D &p2_,double &radius_,tf::TransformListener *listener_,std::string paramFrame_):Shape()
{
    pt1 = p1_;
    pt2 = p2_;
    Vector3D diff = pt1 - pt2;
    length = diff.norm();
    lengthsq = length*length;
    radius = radius_;
    radius_sq = radius*radius;

    diff = diff/diff.norm();
    Vector3D vectorangle(0,0,1);
    vectorangle = vectorangle.cross(diff);
    double sin_angle = vectorangle.norm();
    vectorangle = vectorangle/vectorangle.norm();

    Vector3D yaxis(0,0,1);
    double cos_angle = yaxis.dot(diff);
    double rotation_angle = atan2(sin_angle,cos_angle);
    vectorangle = sin(rotation_angle/2)*vectorangle;
    q.x() = vectorangle.x();
    q.y() = vectorangle.y();
    q.z() = vectorangle.z();
    q.w() = cos(rotation_angle/2);

    paramFrame = paramFrame_;
    //listener = listener_;
};

Cylinder::Cylinder(Vector3D &p1_,Vector3D &p2_,double &radius_,std::string paramFrame_):Shape()
{
    pt1 = p1_;
    pt2 = p2_;
    Vector3D diff = pt1 - pt2;
    length = diff.norm();
    lengthsq = length*length;
    radius = radius_;
    radius_sq = radius*radius;

    diff = diff/diff.norm();
    Vector3D vectorangle(0,0,1);
    vectorangle = vectorangle.cross(diff);
    double sin_angle = vectorangle.norm();
    vectorangle = vectorangle/vectorangle.norm();

    Vector3D yaxis(0,0,1);
    double cos_angle = yaxis.dot(diff);
    double rotation_angle = atan2(sin_angle,cos_angle);
    vectorangle = sin(rotation_angle/2)*vectorangle;
    q.x() = vectorangle.x();
    q.y() = vectorangle.y();
    q.z() = vectorangle.z();
    q.w() = cos(rotation_angle/2);

    paramFrame = paramFrame_;
    //listener = listener_;
};

void Cylinder::changeParams(Vector3D &p1_,Vector3D &p2_,double &radius_)
{
    pt1 = p1_;
    pt2 = p2_;
    Vector3D diff = pt1 - pt2;
    length = diff.norm();
    lengthsq = length*length;
    radius = radius_;
    radius_sq = radius*radius;
};

double Cylinder::inShape(Vector3D &testpt)
{
    Vector3D d;	// vector d  from line segment point 1 to point 2
    Vector3D pd;	// vector pd from point 1 to test point
    double dot_prod, dsq;

    d = pt2 - pt1;	// translate so pt1 is origin.  Make vector from
    // pt1 to pt2.  Need for this is easily eliminated


    pd = testpt - pt1;		// vector from pt1 to test point.

    // Dot the d and pd vectors to see if point lies behind the
    // cylinder cap at pt1.x, pt1.y, pt1.z

    dot_prod = pd.dot(d);

    // If dot is less than zero the point is behind the pt1 cap.
    // If greater than the cylinder axis line segment length squared
    // then the point is outside the other end cap at pt2.

    if( dot_prod < 0.0f || dot_prod > lengthsq )
    {
        return( -1.0f );
    }
    else
    {
        // Point lies within the parallel caps, so find
        // distance squared to the cylinder axis:

        dsq = pd.dot(pd) - dot_prod*dot_prod/lengthsq;

        if( dsq > radius_sq )
        {
            return( -1.0f );
        }
        else
        {
            return( dsq );		// return distance squared to axis
        }
    }
};


visualization_msgs::Marker Cylinder::returnShapeMarker(int id)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = paramFrame;
    marker.header.stamp = ros::Time();
    marker.ns = "cylinders";
    marker.id = id;
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.action = visualization_msgs::Marker::ADD;
    marker.lifetime = ros::Duration(0);


    Vector3D point1 = (pt1 + pt2)/2;
    Eigen::Quaterniond q1;

    q1 = q;
    marker.pose.position.x = point1.x();
    marker.pose.position.y = point1.y();
    marker.pose.position.z = point1.z();
    marker.pose.orientation.x = q1.x();
    marker.pose.orientation.y = q1.y();
    marker.pose.orientation.z = q1.z();
    marker.pose.orientation.w = q1.w();
    marker.scale.x = 2*radius;
    marker.scale.y = 2*radius;
    marker.scale.z = length;
    marker.color.a = 0.35;
    marker.color.r = 0.6;
    marker.color.g = 1.0;
    marker.color.b = 0.6;

    return marker;
};


Sphere::Sphere(Vector3D &center_,double &radius_,tf::TransformListener *listener_)
{
    center = center_;
    radius = radius_;
    radius_sq = radius*radius;
    listener = listener_;
};
void Sphere::changeParams(Vector3D &center_,double &radius_)
{
    center = center_;
    radius = radius_;
    radius_sq = radius*radius;
};

double Sphere::inShape(Vector3D &testpt)
{
    Vector3D length_vec = center - testpt;
    double length = length_vec.squaredNorm();

    if(length<radius_sq)
    {
        return length;
    }

    return -1.0;

};

void Sphere::getParameters(std::vector<double> &params)
{
    params.push_back(center.x());
    params.push_back(center.y());
    params.push_back(center.z());
    params.push_back(radius);
};

visualization_msgs::Marker Sphere::returnShapeMarker(int id)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/world";
    marker.header.stamp = ros::Time();
    marker.ns = "spheres";
    marker.id = id;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.lifetime = ros::Duration(1.0);
    /*
    tf::StampedTransform transform;
    try{
      listener->lookupTransform("/world", "/world_view",
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }*/

    marker.pose.position.x = center.x();// - transform.getOrigin().x();
    marker.pose.position.y = center.y();// - transform.getOrigin().y();
    marker.pose.position.z = center.z(); //- transform.getOrigin().z());
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 2*radius;
    marker.scale.y = 2*radius;
    marker.scale.z = 2*radius;
    marker.color.a = 0.35;
    marker.color.r = 1.0;
    marker.color.g = 0.5;
    marker.color.b = 0.5;

    return marker;
};

Sector::Sector(Vector3D &center_,double & Radius,double& StartAngle,double& EndAngle,Eigen::Quaterniond &Orientation, std::string Frame,std_msgs::ColorRGBA &c)
{
    center = center_;
    radius = Radius;
    setStartEndAngle(StartAngle,EndAngle);
    orientation = Orientation;
    frame = Frame;
    color = c;
};

visualization_msgs::Marker Sector::returnShapeMarker(int id)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "Sector";
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 5.0;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color = color;

    double resolution = (endAngle - startAngle)/4.0;
    Eigen::Quaterniond q;
    q.x() = 0.0;
    q.y() = 0.0;
    Vector3D x(1,0,0);
    Vector3D v;
    geometry_msgs::Point p;
    p = msgcp(center);
    marker.points.push_back(p);
    for(double angle = startAngle; angle >= endAngle; angle =angle+resolution )
    {
        v = x;
        q.z() = sin(angle/2);
        q.w() = cos(angle/2);
        v = q*v;
        v = orientation*v;
        v = radius*v;
        p=msgcp(v);
        marker.points.push_back(p);

    }
    p = msgcp(center);
    marker.points.push_back(p);

    return marker;
};

Frustrum::Frustrum()
{
    surfaceIds[0][0]=1;
    surfaceIds[0][1]=3;
    surfaceIds[0][2]=0;
    surfaceIds[0][3]=2;
    surfaceIds[1][0]=5;
    surfaceIds[1][1]=7;
    surfaceIds[1][2]=4;
    surfaceIds[1][3]=6;
    surfaceIds[2][0]=1;
    surfaceIds[2][1]=6;
    surfaceIds[2][2]=2;
    surfaceIds[2][3]=5;
    surfaceIds[3][0]=3;
    surfaceIds[3][1]=6;
    surfaceIds[3][2]=2;
    surfaceIds[3][3]=7;
    surfaceIds[4][0]=0;
    surfaceIds[4][1]=7;
    surfaceIds[4][2]=3;
    surfaceIds[4][3]=4;
    surfaceIds[5][0]=0;
    surfaceIds[5][1]=5;
    surfaceIds[5][2]=1;
    surfaceIds[5][3]=4;
}

Frustrum::Frustrum(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_,std_msgs::ColorRGBA &color_,std::string paramFrame_,std::string ns_)
{
    surfaceIds[0][0]=1;
    surfaceIds[0][1]=3;
    surfaceIds[0][2]=0;
    surfaceIds[0][3]=2;
    surfaceIds[1][0]=5;
    surfaceIds[1][1]=7;
    surfaceIds[1][2]=4;
    surfaceIds[1][3]=6;
    surfaceIds[2][0]=1;
    surfaceIds[2][1]=6;
    surfaceIds[2][2]=2;
    surfaceIds[2][3]=5;
    surfaceIds[3][0]=3;
    surfaceIds[3][1]=6;
    surfaceIds[3][2]=2;
    surfaceIds[3][3]=7;
    surfaceIds[4][0]=0;
    surfaceIds[4][1]=7;
    surfaceIds[4][2]=3;
    surfaceIds[4][3]=4;
    surfaceIds[5][0]=0;
    surfaceIds[5][1]=5;
    surfaceIds[5][2]=1;
    surfaceIds[5][3]=4;
    changeParams(top_,bottom_);
    setFrame(paramFrame_);
    color = color_;
    ns = ns_;

};

void Frustrum::changeProperties(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_,std_msgs::ColorRGBA &color_,std::string  paramFrame_ ,std::string ns_)
{
    changeParams(top_,bottom_);
    setFrame(paramFrame_);
    setColor(color_);
    ns = ns_;
};

void Frustrum::changeParams(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_)
{
    if(top_.size()!=4)
    {
        ROS_ERROR_STREAM("Frustrum:: Top input vector is not of size 4");
        return;
    }
    if(bottom_.size()!=4)
    {
        ROS_ERROR_STREAM("Frustrum:: Top input vector is not of size 4");
        return;
    }


    vertices.clear();
    for(unsigned int i=0; i<8; i++)
    {
        if(i<4)
        {
            vertices.push_back(top_[i]);
        }
        else
        {
            vertices.push_back(bottom_[(i-4)]);
        }
    }

    normals.clear();
    // getting inside point
    insidePoint.x() =0.0;
    insidePoint.y() =0.0;
    insidePoint.z() =0.0;
    Vector3D ip = insidePoint;
    for(unsigned int i=0; i<vertices.size(); i++)
    {
        insidePoint = ip + vertices[i]/8;
        ip = insidePoint;
    }

    //ROS_INFO_STREAM("insidePoint::"<<insidePoint.x()<<" : "<<insidePoint.y()<<" : "<<insidePoint.z());
    for(unsigned int i=0; i<6; i++)
    {
        std::vector<Vector3D> tVec;
        for(unsigned int j=0; j<4; j++)
        {
            unsigned int id = surfaceIds[i][j];
            tVec.push_back(vertices[id]);
        }
        Vector3D normal_;
        double intercept_;
        getPlaneEquation(tVec,intercept_,normal_);
        normals.push_back(normal_);
        intercepts.push_back(intercept_);
        tVec.clear();
        insidePlaneHalfs.push_back((normal_.dot(insidePoint)-intercept_));
    }

};

Vector3D Frustrum::getPlaneNormal(std::vector<Vector3D>& vertices_)
{
    Vector3D v1=vertices_[0] - vertices_[1];
    Vector3D v2=vertices_[2] - vertices_[3];
    Vector3D n = v1.cross(v2);
    n = n/n.norm();
    return n;

}

void Frustrum::getPlaneEquation(std::vector<Vector3D>& vertices_,double& intercept,Vector3D &normal)
{
    normal = getPlaneNormal(vertices_);
    Vector3D vIn=(vertices_[0] + vertices_[1]+vertices_[2] + vertices_[3])/4;
    intercept = normal.dot(vIn);
}

double Frustrum::inShape(Vector3D &testPt)
{


    for(unsigned int i=0; i<6; i++)
    {
        double inout = (normals[i].dot(testPt)-intercepts[i])*insidePlaneHalfs[i];
        if(inout<0)
        {
            //ROS_INFO_STREAM("Inout::"<<inout);
            return -1.0;
        }
    }

    return 1.0;
}

visualization_msgs::Marker Frustrum::returnShapeMarker(int id)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = paramFrame;
    marker.header.stamp = ros::Time();
    marker.ns = ns;
    marker.id = id;
    marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;
    marker.color = color;

    for(unsigned int i=2; i<6; i++)
    {
        marker.points.push_back(msgcp(vertices[surfaceIds[i][0]]));
        marker.points.push_back(msgcp(vertices[surfaceIds[i][1]]));
        marker.points.push_back(msgcp(vertices[surfaceIds[i][2]]));

        marker.points.push_back(msgcp(vertices[surfaceIds[i][0]]));
        marker.points.push_back(msgcp(vertices[surfaceIds[i][1]]));
        marker.points.push_back(msgcp(vertices[surfaceIds[i][3]]));
    }

    return marker;
}

Frustrum::~Frustrum()
{
    vertices.clear();
    normals.clear();
    intercepts.clear();
    insidePlaneHalfs.clear();
};


