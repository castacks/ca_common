/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2013 Sanjiban Choudhury
 * vector_math.cpp
 *
 *  Created on: Dec 21, 2013
 *      Author: sanjiban
 */

#include "ca_common/math_utils.h"
using namespace CA;

namespace nu = math_utils::numeric_operations;

double math_utils::vector_math::Angle(const Vector3D &vec) {
  return atan2(vec.y(), vec.x());
}

double math_utils::vector_math::Angle(const Vector3D  &from, const Vector3D &to) {
  return math_utils::angular_math::WrapToPi(Angle(to) - Angle(from));
}

double math_utils::vector_math::Length(const Vector3D &vec) {
  return sqrt(vec.x()*vec.x() + vec.y()*vec.y() + vec.z()*vec.z());
}


double math_utils::vector_math::Length2D(const Vector3D &vec) {
  return sqrt(vec.x()*vec.x() + vec.y()*vec.y());
}

bool math_utils::vector_math::Equal2D(const Vector3D &vec1, const Vector3D &vec2) {
  return math_utils::vector_math::Length2D(vec1 - vec2) < std::numeric_limits<double>::epsilon();
}

double math_utils::vector_math::Cross2D(const Vector3D &vec1, const Vector3D &vec2) {
  return vec1.x()*vec2.y() - vec1.y()*vec2.x();
}

Vector3D math_utils::vector_math::VectorToLine(const Vector3D &line_start, const Vector3D &line_end, const Vector3D &candidate) {
  return (line_start - candidate) + (((candidate - line_start).dot(line_end - line_start))/((line_end-line_start).dot(line_end-line_start)))*(line_end-line_start);
}

bool math_utils::vector_math::Intersection2D(const Vector3D &from_start, const Vector3D &from_end, const Vector3D &to_start, const Vector3D &to_end, Vector3D &intersection) {
  if (math_utils::vector_math::Equal2D(from_end, to_start)) {
    intersection = from_end;
    return true;
  }

  if (fabs(math_utils::vector_math::Cross2D(from_end - from_start, to_start - to_end)) < std::numeric_limits<double>::epsilon())
    return false;

  double l1 = math_utils::vector_math::Cross2D(to_start - from_end, to_start - to_end) / math_utils::vector_math::Cross2D(from_end - from_start, to_start - to_end);
  double l2 = math_utils::vector_math::Cross2D(to_start - from_end, from_end - from_start) / math_utils::vector_math::Cross2D(from_end - from_start, to_start - to_end);

  if (l1 < 0 || l2 < 0)
    return false;

  intersection = from_end + l1*(from_end - from_start);
  intersection[2] = (l2*from_end[2] + l1*to_start[2])/(l1+l2);
  return true;
}

bool math_utils::vector_math::Intersection2D(const Vector3D &from_start, const Vector3D &from_end, const Vector3D &to_start, const Vector3D &to_end, double offset_limit, Vector3D &intersection) {
  if (!math_utils::vector_math::Intersection2D(from_start, from_end, to_start, to_end, intersection))
    return false;
  Vector3D dir = math_utils::vector_math::VectorToLine(from_end, to_start, intersection);
  if (math_utils::vector_math::Length(dir) > offset_limit)
    intersection += (1 - offset_limit/math_utils::vector_math::Length(dir))*dir;
  return true;
}

Vector3D math_utils::vector_math::Rotate2DByVector(const Vector3D &to_rotate, const Vector3D &rotate_around) {
  double psi = -math_utils::vector_math::Angle(rotate_around);
  Eigen::MatrixXd rot_2d(3,3);
  rot_2d << cos(psi), -sin(psi), 0,
            sin(psi), cos(psi), 0,
            0, 0, 1;
  return rot_2d*to_rotate;
}

double math_utils::vector_math::DistanceSidePlane(const Eigen::Vector3d &origin, const Eigen::Vector3d &to, const Eigen::Vector3d &check) {
  Eigen::Vector3d vec(to - origin);
  vec.normalize();
  Eigen::Vector3d vec2(check - origin);
  return vec.dot(vec2);
}

Eigen::Vector3d math_utils::vector_math::Limit(const Eigen::Vector3d & limit, const Eigen::Vector3d & value) {
    return Eigen::Vector3d ( nu::Limit ( limit[0],value[0] ), nu::Limit ( limit[1],value[1] ), nu::Limit ( limit[2],value[2] ) );
}
