/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2014 Sanjiban Choudhury
 * numeric_operations.cpp
 *
 *  Created on: Jan 6, 2014
 *      Author: sanjiban
 */


#include "ca_common/math_utils.h"

using namespace CA;

int math_utils::numeric_operations::Wrap(int kX, int const kLowerBound, int const kUpperBound) {
    int range_size = kUpperBound - kLowerBound + 1;

    if (kX < kLowerBound)
        kX += range_size * ((kLowerBound - kX) / range_size + 1);

    return kLowerBound + (kX - kLowerBound) % range_size;
}

double math_utils::numeric_operations::Limit ( double limit, double value ) {
    if ( value>limit ) {
        return limit;
    } else {
        if ( value<-limit )
            return -limit;
        else
            return value;
    }
}
