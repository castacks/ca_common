/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2014 Sanjiban Choudhury
 * transform_math.cpp
 *
 *  Created on: Jan 4, 2014
 *      Author: sanjiban
 */


#include "ca_common/math_utils.h"
using namespace CA;

Vector3D math_utils::transform_math::GetStaticVel(const State &sc) {
  Eigen::MatrixXd rot_2d(3,3);
  double psi = sc.pose.orientation_rad[2];
  rot_2d << cos(psi), -sin(psi), 0,
            sin(psi), cos(psi), 0,
            0, 0, 1;
  return rot_2d*sc.rates.velocity_mps;
}

