/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/** Coordinate conversion class: UTM <-> Latitude/Longitude
 *  Author: smaeta
 */

#include <stdio.h>
#include <stdlib.h>
#include <exception>

#include <ros/ros.h>

extern "C"
{
#include <proj_api.h>
}

#include "ca_common/coordconv.h"
#include "ca_common/math.h"

using namespace CA;
using namespace CA::math_consts;
using namespace std;


/** Auxiliar data structure that holds PROJ.4 library data structures
 */
typedef struct {
    projPJ pj_merc;
    projPJ pj_latlong;
} CoordConvData;



/** Exception: UTM Zone is not set - needs to set it in ROS parameters
 *  Zone number (1 to 60) and hemisphere (north / south)
 */
class UTMZoneNotSetException: public exception
{
    virtual const char* what() const throw()
    {
        return "Exception: UTM Zone not set properly.\nNeed to set ROS params:\n"\
               "\tutm_zone_number : 1 to 60\n"\
               "\tutm_hemisphere : 'N' or 'S' (north/south)\n"\
               "OR: use method CoordinateConversion::setUTMZone(int zone, char hemisphere)\n";
    }
} utmZoneNotSetException;


/** Exception: projection data structure not initialized properly.
 *  Probably initialization string is not set properly.
 */
class ProjStructInitException: public exception
{
    virtual const char* what() const throw()
    {
        return "Exception: Projection structure initialization failed.\n"\
               "Initialization parameters not set properly.\n"\
               "Check CoordinateConversion() class implementation.\n";
    }
} projStructInitException;


//==============================================================================
//==============================================================================



CoordinateConversion::CoordinateConversion()
{
    CoordConvData *convData = new CoordConvData;

    //For mode details about how to set coordinate conversion using PROJ.4:
    //    http://trac.osgeo.org/proj/wiki
    //Configuration parameters can be found at:
    //    http://trac.osgeo.org/proj/wiki/GenParms

    if (!(convData->pj_latlong = pj_init_plus("+proj=latlong +ellps=WGS84 +datum=NAD83"))) {
        throw projStructInitException;
    }
    //convData->pj_merc = pj_init_plus("+proj=utm +ellps=WGS84 +datum=NAD83 +zone=18 +north");

    internalData = (void *)convData;

    //No default UTM zone settings - user has to set it
    utmZone = -1;
    utmHemisphere = ' ';

    //setUTMZone(18, 'N');

    updateUTMZoneSettings();
}



CoordinateConversion::~CoordinateConversion()
{
    if (internalData != NULL) {
        CoordConvData *convData;
        convData = (CoordConvData *) internalData;
        delete convData;
        internalData = NULL;
    }
}



void CoordinateConversion::updateUTMZoneSettings(void)
{
    bool hasValidZone = false;
    bool hasValidHemisphere = false;
    int utm_zone;
    char utm_hemisphere;
    if (ros::param::has("utm_zone_number")) {
        ros::param::get("utm_zone_number", utm_zone);
        if ((utm_zone < 1) || (utm_zone > 60)) {
            throw utmZoneNotSetException;
        }
        hasValidZone = true;
    }

    if (ros::param::has("utm_hemisphere")) {
        string auxstr;
        ros::param::get("utm_hemisphere", auxstr);
        utm_hemisphere = auxstr.c_str()[0];
        if ((utm_hemisphere != 's') && (utm_hemisphere != 'S') &&
                (utm_hemisphere != 'n') && (utm_hemisphere != 'N')) {
            throw utmZoneNotSetException;
        }
        hasValidHemisphere = true;
    }

    if ((hasValidZone) && (hasValidHemisphere)) {
        setUTMZone(utm_zone, utm_hemisphere);
    }
}



void CoordinateConversion::checkUTMZoneSettings(void)
{
    if ((utmZone < 1) || (utmZone > 60)) {
        throw utmZoneNotSetException;
    }
    if ((utmHemisphere != 's') && (utmHemisphere != 'S') &&
            (utmHemisphere != 'n') && (utmHemisphere != 'N')) {
        throw utmZoneNotSetException;
    }
}



int CoordinateConversion::setUTMZone(int zone, char hemisphere)
{
    if ((zone < 1) || (zone > 60)) {
        return -1; //Invalid zone number
    }

    if ((hemisphere != 's') && (hemisphere != 'S') &&
            (hemisphere != 'n') && (hemisphere != 'N')) {
        return -2; //Invalid hemisphere
    }

    //Check if it is the same as the previous settings - just return
    if ((zone == utmZone) && (hemisphere == utmHemisphere)) {
        return 0;
    }

    CoordConvData *convData = (CoordConvData *) internalData;

    utmZone = zone;
    utmHemisphere = hemisphere;

    checkUTMZoneSettings();

    char auxstr[1024];
    sprintf(auxstr, "+proj=utm +ellps=WGS84 +datum=NAD83 +zone=%d +%s", utmZone,
            (((utmHemisphere=='N')||(utmHemisphere=='n'))?"north":"south"));

    //fprintf(stderr, "%s\n", auxstr);

    if (!(convData->pj_merc = pj_init_plus(auxstr))) {
        throw projStructInitException;
    }

    return 0; //OK
}



int CoordinateConversion::UTM2LatLongRad(double utmNorth, double utmEast,
        double *lat, double *lon)
{
    int res = 0;
    CoordConvData *convData = (CoordConvData *) internalData;
    if (convData == NULL) return -10;

    //updateUTMZoneSettings();
    checkUTMZoneSettings();
    double x, y, z;
    x = utmEast;
    y = utmNorth;
    z = 0.0;
    res = pj_transform(convData->pj_merc, convData->pj_latlong, 1, 1, &x, &y, &z);
    (*lon) = x;
    (*lat) = y;
    return res; //0==OK
}



int CoordinateConversion::LatLongRad2UTM(double lat, double lon,
        double *utmNorth, double *utmEast)
{
    int res = 0;
    CoordConvData *convData = (CoordConvData *) internalData;
    if (convData == NULL) return -10;

    //updateUTMZoneSettings();
    checkUTMZoneSettings();
    double x, y, z;
    x = lon;
    y = lat;
    z = 0.0;
    res = pj_transform(convData->pj_latlong, convData->pj_merc, 1, 1, &x, &y, &z);
    (*utmEast) = x;
    (*utmNorth) = y;
    return res; //0==OK
}



int CoordinateConversion::LatLongDeg2UTM(double latDeg, double latMin, double latSec,
        double lonDeg, double lonMin, double lonSec,
        double *utmNorth, double *utmEast)
{
    CoordConvData *convData = (CoordConvData *) internalData;
    if (convData == NULL) return -10;

    //updateUTMZoneSettings();
    checkUTMZoneSettings();
    double lat, lon;

    lat = (latDeg + latMin/60.0 + latSec/3600.0) * DEG2RAD;
    lon = (lonDeg + lonMin/60.0 + lonSec/3600.0) * DEG2RAD;

    //fprintf(stderr, "%f %f\n", lat * RAD2DEG, lon * RAD2DEG);

    return LatLongRad2UTM(lat, lon, utmNorth, utmEast);
}

int CoordinateConversion::UTM2LatLongRad(const std::vector<double> &utmNorth_arr,
        const std::vector<double> &utmEast_arr,
        std::vector<double> &lat_arr,
        std::vector<double> &lon_arr)
{
    int res = 0;
    CoordConvData *convData = (CoordConvData *) internalData;
    if (convData == NULL) return -10;

    //updateUTMZoneSettings();
    checkUTMZoneSettings();
    assert(utmNorth_arr.size() == utmEast_arr.size());

    lat_arr.clear();
    lat_arr.resize(utmNorth_arr.size());
    lon_arr.clear();
    lon_arr.resize(utmNorth_arr.size());
    for (unsigned int i = 0; i<utmNorth_arr.size(); i++) {
        double x, y, z;
        x = utmEast_arr[i];
        y = utmNorth_arr[i];
        z = 0.0;
        res += pj_transform(convData->pj_merc, convData->pj_latlong, 1, 1, &x, &y, &z);
        lon_arr[i] = x;
        lat_arr[i] = y;
    }
    return res; //0==OK
}

int CoordinateConversion::LatLongRad2UTM(const std::vector<double> &lat_arr,
        const std::vector<double> &lon_arr,
        std::vector<double> &utmNorth_arr,
        std::vector<double> &utmEast_arr)
{
    int res = 0;
    CoordConvData *convData = (CoordConvData *) internalData;
    if (convData == NULL) return -10;

    //updateUTMZoneSettings();
    checkUTMZoneSettings();
    assert(lat_arr.size() == lon_arr.size());

    utmEast_arr.clear();
    utmEast_arr.resize(lat_arr.size());
    utmNorth_arr.clear();
    utmNorth_arr.resize(lat_arr.size());

    for (unsigned int i = 0; i<lat_arr.size(); i++) {
        double x, y, z;
        x = lon_arr[i];
        y = lat_arr[i];
        z = 0.0;
        res += pj_transform(convData->pj_latlong, convData->pj_merc, 1, 1, &x, &y, &z);
        utmEast_arr[i] = x;
        utmNorth_arr[i] = y;
    }

    return res; //0==OK
}

