/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ca_common/math.h>
#include <cmath>

#include <boost/foreach.hpp>
#include <boost/assert.hpp>

#include <ca_common/Trajectory.h>
#include <ca_common/TrajectoryPoint.h>

using namespace CA;
using namespace std;

const Vector3D ZERO_3D(0,0,0);
const QuatD ZEROROT_D(1,0,0,0);

// TODO why not boost::math::constants?
const double math_consts::SQRT2OVER2  (0.7071067811865475);
const double math_consts::DEG2RAD     (M_PI/180.0);
const double math_consts::RAD2DEG     (180.0/M_PI);
const double math_consts::KNT2MS      (0.514);
const double math_consts::FEETPMIN2MS (0.00508);
const double math_consts::GRAVITY     (9.81);


void Trajectory::setLinear(std::vector<State,Eigen::aligned_allocator<Vector3D> > &tt)
{
    isLinear = true;
    c.clear();
    t = tt;
}

void Trajectory::setCoefs(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset)
{
    isLinear = false;
    //isLinear = true;
    c = splines;
    segtimes = times;
    m_timeoffset = timeoffset;
    spline2t();
    currentPrecision = 1;
}

void Trajectory::setCoefs4d(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset)
{
    isLinear = false;
    //isLinear = true;
    c = splines;
    segtimes = times;
    m_timeoffset = timeoffset;
    spline2t4d();
    currentPrecision = 1;
}

void Trajectory::setCoefs(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset, double precision)
{
    isLinear = false;
    //isLinear = true;
    c = splines;
    segtimes = times;
    m_timeoffset = timeoffset;
    spline2t(precision);
    currentPrecision = precision;
}

void Trajectory::changePrecision(double precision)
{
    spline2t(precision);
    currentPrecision = precision;
}

std::vector<Spline1D> Trajectory::getCoefs()
{
    return c;
}

const std::vector<State, Eigen::aligned_allocator<Vector3D> >& Trajectory::getLinear()const
{
    return t;
}

int Trajectory::indexCount()
{
    if(isLinear)
        return t.size()-1;

    return 1;
}

void Trajectory::spline2t()
{
    t.clear();
    //TODO currently assumes all Spline1D's have the same size
    int segnum = c[0].size();

    State sc;
    for(int seg=0; seg<segnum; seg++)
    {
        Eigen::Vector3d dxvec(c[0][seg][0],c[0][seg][1],c[0][seg][2]);
        Eigen::Vector3d dyvec(c[1][seg][0],c[1][seg][1],c[1][seg][2]);
        Eigen::Vector3d dzvec(c[2][seg][0],c[2][seg][1],c[2][seg][2]);

        double tstep = (segtimes[seg+1] - segtimes[seg])/4;

        for(int j=0; j<4; j++)
        {
            double i = segtimes[seg] + j*tstep;
            //This is assuming a spline with polynomials of order 3
            Eigen::Vector4d tvec(i*i*i,i*i,i,1);
            Eigen::Vector3d dtvec(3*i*i,2*i,1);

            sc.pose.position_m[0] = tvec.dot(c[0][seg]);
            sc.pose.position_m[1] = tvec.dot(c[1][seg]);
            sc.pose.position_m[2] = tvec.dot(c[2][seg]);

            sc.rates.velocity_mps[0] = dtvec.dot(dxvec);
            sc.rates.velocity_mps[1] = dtvec.dot(dyvec);
            sc.rates.velocity_mps[2] = dtvec.dot(dzvec);

            sc.pose.orientation_rad[2] = atan2(sc.rates.velocity_mps[1],sc.rates.velocity_mps[0]);
            sc.time_s = i;
            t.push_back(sc);
        }
    }
}

void Trajectory::spline2t4d()
{
    t.clear();
    //TODO currently assumes all Spline1D's have the same size
    int segnum = c[0].size();

    State sc;
    for(int seg=0; seg<segnum; seg++)
    {
        Eigen::Vector3d dxvec(c[0][seg][0],c[0][seg][1],c[0][seg][2]);
        Eigen::Vector3d dyvec(c[1][seg][0],c[1][seg][1],c[1][seg][2]);
        Eigen::Vector3d dzvec(c[2][seg][0],c[2][seg][1],c[2][seg][2]);
        Eigen::Vector3d dpsivec(c[3][seg][0],c[3][seg][1],c[3][seg][2]);

        double tstep = (segtimes[seg+1] - segtimes[seg])/4;

        for(int j=0; j<4; j++)
        {
            double i = segtimes[seg] + j*tstep;
            //This is assuming a spline with polynomials of order 3
            Eigen::Vector4d tvec(i*i*i,i*i,i,1);
            Eigen::Vector3d dtvec(3*i*i,2*i,1);

            sc.pose.position_m[0] = tvec.dot(c[0][seg]);
            sc.pose.position_m[1] = tvec.dot(c[1][seg]);
            sc.pose.position_m[2] = tvec.dot(c[2][seg]);

            sc.rates.velocity_mps[0] = dtvec.dot(dxvec);
            sc.rates.velocity_mps[1] = dtvec.dot(dyvec);
            sc.rates.velocity_mps[2] = dtvec.dot(dzvec);

            sc.pose.orientation_rad[2] = tvec.dot(c[3][seg]);
            sc.rates.rot_rad[2] = dtvec.dot(dpsivec);
            sc.time_s = i;
            t.push_back(sc);
        }
    }
}


void Trajectory::spline2t(double precision)
{
    t.clear();
    //TODO currently assumes all Spline1D's have the same size
    int segnum = c[0].size();

    State sc;
    for(int seg=0; seg<segnum; seg++)
    {
        Eigen::Vector3d dxvec(c[0][seg][0],c[0][seg][1],c[0][seg][2]);
        Eigen::Vector3d dyvec(c[1][seg][0],c[1][seg][1],c[1][seg][2]);
        Eigen::Vector3d dzvec(c[2][seg][0],c[2][seg][1],c[2][seg][2]);
        Eigen::Vector3d crossvec(-6*c[0][seg][0]*c[1][seg][1] + 6*c[1][seg][0]*c[0][seg][1],- 6*c[0][seg][0]*c[1][seg][2] + 6*c[1][seg][0]*c[0][seg][2],2*c[1][seg][1]*c[0][seg][2] - 2*c[0][seg][1]*c[1][seg][2]);
        double i = 0;
        while(i<1.0)
        {
            //This is assuming a spline with polynomials of order 3
            Eigen::Vector4d tvec(i*i*i,i*i,i,1);
            Eigen::Vector3d dtvec(3*i*i,2*i,1);
            Eigen::Vector3d tvec2d(i*i,i,1);

            sc.pose.position_m[0] = tvec.dot(c[0][seg]);
            sc.pose.position_m[1] = tvec.dot(c[1][seg]);
            sc.pose.position_m[2] = tvec.dot(c[2][seg]);

            sc.rates.velocity_mps[0] = dtvec.dot(dxvec);
            sc.rates.velocity_mps[1] = dtvec.dot(dyvec);
            sc.rates.velocity_mps[2] = dtvec.dot(dzvec);

            sc.pose.orientation_rad[2] = atan2(sc.rates.velocity_mps[1],sc.rates.velocity_mps[0]);
            sc.rates.rot_rad[2] = tvec2d.dot(crossvec)/(sc.rates.velocity_mps[0]*sc.rates.velocity_mps[0]+sc.rates.velocity_mps[1]*sc.rates.velocity_mps[1]);

            sc.time_s = i;

            t.push_back(sc);

            i += precision/math_tools::Length(sc.rates.velocity_mps);
        }

        Eigen::Vector4d tvec(1,1,1,1);
        Eigen::Vector3d dtvec(3,2,1);

        sc.pose.position_m[0] = tvec.dot(c[0][seg]);
        sc.pose.position_m[1] = tvec.dot(c[1][seg]);
        sc.pose.position_m[2] = tvec.dot(c[2][seg]);

        sc.rates.velocity_mps[0] = dtvec.dot(dxvec);
        sc.rates.velocity_mps[1] = dtvec.dot(dyvec);
        sc.rates.velocity_mps[2] = dtvec.dot(dzvec);

        sc.pose.orientation_rad[2] = atan2(sc.rates.velocity_mps[1],sc.rates.velocity_mps[0]);
        sc.time_s = 1;
        t.push_back(sc);

    }
}

std::vector<std::pair<State,double> > Trajectory::sampleAt(int index, float resolution)const
{
    std::vector<std::pair<State,double> > samples;
    State s;
    std::pair<State,int> sample;
    int numBack = 0;
    int numFwd = 0;
    double length;
    Vector3D vecBack, vecFwd;

    if(index < 0 || index >= (int)(t.size())) //index out of bounds
    {
        return samples;
    }

    if(index != 0) //we can go backwards
    {
        vecBack = t[index-1].pose.position_m - t[index].pose.position_m;
        length = math_tools::Length(vecBack)/2;
        numBack = round(length/resolution);
        math_tools::normalize(vecBack);
    }

    if(index != (int)(t.size()-1)) //we can go forwards
    {
        vecFwd = t[index+1].pose.position_m - t[index].pose.position_m;
        length = math_tools::Length(vecFwd)/2;
        numFwd = round(length/resolution);
        math_tools::normalize(vecFwd);
    }

    int maxWeight = std::max(numBack,numFwd) + 1;
    int weightSum = 0;

    //visit the actual trajectory node
    sample.first = t[index];
    sample.second = maxWeight;
    weightSum += sample.second;
    samples.push_back(sample);

    //sample backwards
    if(numBack > 0)
    {
        for(int i = 1; i <= numBack; i++)
        {
            s.pose.position_m = t[index].pose.position_m + i*vecBack*resolution;
            sample.first = s;
            sample.second = maxWeight - i;
            weightSum += sample.second;
            samples.push_back(sample);
        }
    }

    //sample forwards
    if(numFwd > 0)
    {
        for(int j = 1; j <= numFwd; j++)
        {
            s.pose.position_m = t[index].pose.position_m + j*vecFwd*resolution;
            sample.first = s;
            sample.second = maxWeight - j;
            weightSum += sample.second;
            samples.push_back(sample);
        }
    }

    //normalize weights
    for(unsigned int k = 0; k < samples.size(); k++)
    {
        samples[k].second = samples[k].second/weightSum;
    }

    return samples;

}

State Trajectory::stateAt(int index)const
{
    return t[index];
}

State Trajectory::stateAt(double index, bool extrapolate)const
{
    State sc;

    int prev = std::floor(index);
    int next = std::ceil(index);

    if(next > (int)t.size()-1)
    {
        // Extrapolate
        //\TODO
        return sc;
    }

    State a = t[prev];
    State b = t[next];
    double prog = index - (double)prev;

    sc = math_tools::interpolate(prog, a, b);

    return sc;

}

State Trajectory::stateAtTime(double time_start, bool extrapolate)const
{
    State sc;
    ROS_ERROR_STREAM("this is wrong!!!!!!!!!!!!!!!11111");
    /*
      if(isLinear)
      {
    	  time_start = 100;

    	  double starttime = t[0].time_s;

    	  if(t.size()>=1)
    	  {
    		  for(unsigned int i=1;i<t.size();i++)
    		  {
    				double newtime = t[i].time_s;
    				double deltat = newtime - starttime;
    				if(deltat>time_start)
    				{
    					State a = t[i-1];
    					State b = t[i];
    					 double prog = (time_start-(t[i-1].time_s-starttime))/(t[i].time_s-t[i-1].time_s);

    					  sc.pose.position_m        = (1 - prog) * a.pose.position_m        + (prog) * b.pose.position_m;
    					  sc.pose.orientation_rad   = (1 - prog) * a.pose.orientation_rad   + (prog) * b.pose.orientation_rad;
    					  sc.rates.velocity_mps     = (1 - prog) * a.rates.velocity_mps     + (prog) * b.rates.velocity_mps;
    					  sc.rates.rot_rad          = (1 - prog) * a.rates.rot_rad          + (prog) * b.rates.rot_rad;
    					  sc.pose.position_m      = (1 - prog) * a.pose.position_m      + (prog) * b.pose.position_m;
    					  sc.pose.orientation_rad = (1 - prog) * a.pose.orientation_rad + (prog) * b.pose.orientation_rad;
    					  sc.rates.velocity_mps   = (1 - prog) * a.rates.velocity_mps   + (prog) * b.rates.velocity_mps;
    					  sc.rates.rot_rad        = (1 - prog) * a.rates.rot_rad        + (prog) * b.rates.rot_rad;
    					  sc.main_rotor_speed     = (1 - prog) * a.main_rotor_speed     + (prog) * b.main_rotor_speed;
    					  return sc;
    				}

    		  }

    		  sc = t[t.size()-1];
    	  }
      }
      else
      {
    	  int seg=0;
    	  int segnum = c[0].size();
    	  double ts = time_start - m_timeoffset;

    	  if(ts <= segtimes[0])
    	  {
    		  seg = 0;
    		  ts = segtimes[0];
    	  }
    	  else if(ts >= segtimes.back())
    	  {
    		  seg = segnum;
    	  }
    	  else
    	  {
    		  for(int i=0; i<(int)segnum; i++)
    		  {
    			  if(ts > segtimes[i] && ts < segtimes[i+1])
    			  {
    				  seg = i;
    			  }
    		  }
    	  }

    		Eigen::Vector3d dxvec(c[0][seg][0],c[0][seg][1],c[0][seg][2]);
    		Eigen::Vector3d dyvec(c[1][seg][0],c[1][seg][1],c[1][seg][2]);
    		Eigen::Vector3d dzvec(c[2][seg][0],c[2][seg][1],c[2][seg][2]);


    		//This is assuming a spline with polynomials of order 3
    		Eigen::Vector4d tvec(ts*ts*ts,ts*ts,ts,1);
    		Eigen::Vector3d dtvec(3*ts*ts,2*ts,1);

    		sc.pose.position_m[0] = tvec.dot(c[0][seg]);
    		sc.pose.position_m[1] = tvec.dot(c[1][seg]);
    		sc.pose.position_m[2] = tvec.dot(c[2][seg]);

    		sc.rates.velocity_mps[0] = dtvec.dot(dxvec);
    		sc.rates.velocity_mps[1] = dtvec.dot(dyvec);
    		sc.rates.velocity_mps[2] = dtvec.dot(dzvec);

    		sc.pose.orientation_rad[2] = atan2(sc.rates.velocity_mps[1],sc.rates.velocity_mps[0]);
    		sc.time_s = time_start;
      }
    */
    return sc;
}

State Trajectory::sampleAtTime(double sample_time)const
{
    State sc;
    if(isLinear)
    {
        if(t.size()>=1)
        {
            for(unsigned int i=0; i<t.size()-1; i++)
            {
                State a = t[i];
                State b = t[i+1];
                if(sample_time<a.time_s)
                {
                    return a;
                }
                else if(sample_time>=a.time_s && sample_time<b.time_s)
                {
                    double prog = (sample_time-a.time_s)/(b.time_s-a.time_s);
                    State sc;
                    sc.time_s = (1-prog)*a.time_s + prog * b.time_s;
                    sc.pose.position_m      = (1 - prog) * a.pose.position_m      + (prog) * b.pose.position_m;
                    for(int j=0; j<3; j++)
                    {
                        double diff = fabs(a.pose.orientation_rad[j] - b.pose.orientation_rad[j]);
                        double orient_1 = (diff>M_PI && a.pose.orientation_rad[j] < 0) ? a.pose.orientation_rad[j] + 2*M_PI : a.pose.orientation_rad[j];
                        double orient_2 = (diff>M_PI && b.pose.orientation_rad[j] < 0) ? b.pose.orientation_rad[j] + 2*M_PI : b.pose.orientation_rad[j];
                        sc.pose.orientation_rad[j] = math_tools::limit2pi((1 - prog) * orient_1 + (prog) * orient_2);
                    }

                    sc.rates.velocity_mps   = (1 - prog) * a.rates.velocity_mps   + (prog) * b.rates.velocity_mps;
                    sc.rates.rot_rad        = (1 - prog) * a.rates.rot_rad        + (prog) * b.rates.rot_rad;
                    sc.main_rotor_speed     = (1 - prog) * a.main_rotor_speed     + (prog) * b.main_rotor_speed;
                    return sc;
                }
            }
            sc = t[t.size()-1];
        }
    }


    return sc;
}

Trajectory Trajectory::segmentToTime(double time_start)const
{
    Trajectory traj;
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t_traj;
    if(isLinear)
    {
        if(t.size()>=1)
        {
            for(unsigned int i=0; i<t.size()-1; i++)
            {
                State a = t[i];
                State b = t[i+1];
                if(time_start<a.time_s)
                {
                    traj.setLinear(t_traj);
                    return traj;
                }
                else if(time_start==a.time_s)
                {
                    t_traj.push_back(a);
                    traj.setLinear(t_traj);
                    return traj;
                }
                else if(time_start>a.time_s && time_start<b.time_s)
                {
                    double prog = (time_start-a.time_s)/(b.time_s-a.time_s);
                    State sc;
                    sc.time_s = (1-prog)*a.time_s + prog * b.time_s;
                    sc.pose.position_m      = (1 - prog) * a.pose.position_m      + (prog) * b.pose.position_m;
                    for(int j=0; j<3; j++)
                    {
                        double orient_1 = a.pose.orientation_rad[j] < 0 ? a.pose.orientation_rad[j] + 2*M_PI : a.pose.orientation_rad[j];
                        double orient_2 = b.pose.orientation_rad[j] < 0 ? b.pose.orientation_rad[j] + 2*M_PI : b.pose.orientation_rad[j];
                        sc.pose.orientation_rad[j] = math_tools::limit2pi((1 - prog) * orient_1 + (prog) * orient_2);
                    }

                    sc.rates.velocity_mps   = (1 - prog) * a.rates.velocity_mps   + (prog) * b.rates.velocity_mps;
                    sc.rates.rot_rad        = (1 - prog) * a.rates.rot_rad        + (prog) * b.rates.rot_rad;
                    sc.main_rotor_speed     = (1 - prog) * a.main_rotor_speed     + (prog) * b.main_rotor_speed;
                    t_traj.push_back(a);
                    t_traj.push_back(sc);
                    traj.setLinear(t_traj);
                    return traj;
                }
                else
                    t_traj.push_back(a);
            }

            t_traj.push_back(t[t.size()-1]);
            traj.setLinear(t_traj);
        }
    }


    return traj;
}

Trajectory Trajectory::segmentFromTime(double time_start)const
{
    Trajectory traj;
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t_traj;
    if(isLinear)
    {
        if(t.size()>=1)
        {
            if(time_start<t[0].time_s)
            {
                t_traj.push_back(t[0]);
            }
            for(unsigned int i=0; i<t.size()-1; i++)
            {
                State a = t[i];
                State b = t[i+1];
                if(time_start>b.time_s)
                {
                    continue;
                }
                else if(time_start==b.time_s)
                {
                    t_traj.push_back(b);
                }
                else if(time_start>a.time_s && time_start<b.time_s)
                {
                    double prog = (time_start-a.time_s)/(b.time_s-a.time_s);
                    State sc;
                    sc.time_s = (1-prog)*a.time_s + prog * b.time_s;
                    sc.pose.position_m      = (1 - prog) * a.pose.position_m      + (prog) * b.pose.position_m;
                    for(int j=0; j<3; j++)
                    {
                        double orient_1 = a.pose.orientation_rad[j] < 0 ? a.pose.orientation_rad[j] + 2*M_PI : a.pose.orientation_rad[j];
                        double orient_2 = b.pose.orientation_rad[j] < 0 ? b.pose.orientation_rad[j] + 2*M_PI : b.pose.orientation_rad[j];
                        sc.pose.orientation_rad[j] = math_tools::limit2pi((1 - prog) * orient_1 + (prog) * orient_2);
                    }

                    sc.rates.velocity_mps   = (1 - prog) * a.rates.velocity_mps   + (prog) * b.rates.velocity_mps;
                    sc.rates.rot_rad        = (1 - prog) * a.rates.rot_rad        + (prog) * b.rates.rot_rad;
                    sc.main_rotor_speed     = (1 - prog) * a.main_rotor_speed     + (prog) * b.main_rotor_speed;
                    t_traj.push_back(sc);
                    t_traj.push_back(b);
                }
                else
                    t_traj.push_back(b);
            }
            traj.setLinear(t_traj);
        }
    }

    return traj;
}


bool Trajectory::isfinite() const
{
    unsigned ts = t.size();
    for(unsigned int it = 0; it < ts; it++)
    {
        bool f = CA::isfinite(t[it]);
        if(!f)
            return false;
    }
    return true;
}

State Trajectory::projectOnTrajInterp(State s, bool * atEnd, double * closestIdx)
{

    State newState=s;

    Vector3D minVec(0,0,0);
    double minL = numeric_limits<double>::max();
    unsigned ts = t.size();
    double minIdx = 0.0;
    unsigned int startI=0;
    if(closestIdx != NULL)
    {
        minIdx = *closestIdx;
        minIdx = std::max(minIdx,0.0);
        startI = std::floor(minIdx);
        startI = std::min(startI,ts-1);
        newState = t[ts-1];
    }
    if(ts == 1)
    {
        minIdx = 0;
        newState = t[0];
    } else if(ts>1)
    {
        for(unsigned int it = startI; it < (ts-1); it++)
        {
            Vector3D vec;
            bool onEnd;
            double rv;
            State A = t[ it     ];
            State B = t[ it + 1 ];

            vec = math_tools::vectorToLineSegment(A.pose.position_m, B.pose.position_m, s.pose.position_m, &onEnd, &rv);
            double len = math_tools::Length(vec);
            if (len < minL)
            {
                minL = len;

                if(rv < 0.0) rv = 0.0;
                if(rv > 1.0) rv = 1.0;

                minIdx = it + rv;
                newState = math_tools::interpolate(rv,A,B);
            }
            if(len >= minL && minL != numeric_limits<double>::max())
            {
                break;
            }
        }
    }

    if(atEnd != NULL)
    {
        if(minIdx == (ts-1))
        {
            *atEnd = true;
        }
        else
        {
            *atEnd = false;
        }
    }

    if(closestIdx != NULL)
    {
        *closestIdx = minIdx;
    }
    return newState;
}

State Trajectory::projectOnTrajInterpPerpendicular(State s, bool * atEnd, double * closestIdx)
{

    State newState=s;

    Vector3D minVec(0,0,0);
    //double minL = numeric_limits<double>::max();
    unsigned ts = t.size();
    double minIdx = 0.0;
    unsigned int startI=0;
    if(closestIdx != NULL)
    {
        minIdx = *closestIdx;
        minIdx = std::max(minIdx,0.0);
        startI = std::floor(minIdx);
        startI = std::min(startI,ts-1);
        newState = t[ts-1];
    }
    if(ts == 1)
    {
        minIdx = 0;
        newState = t[0];
    } else if(ts>1)
    {
        State start = t[ 0  ];
        State end = t[ ts-1 ];
        Vector3D dir = end.pose.position_m - start.pose.position_m;
        math_tools::normalize(dir);

        for(unsigned int it = startI; it < (ts-1); it++)
        {
            Vector3D vec;
            bool onEnd;
            double rv;
            State A = t[ it     ];
            State B = t[ it + 1 ];

            vec = math_tools::vectorToLineSegment(A.pose.position_m, B.pose.position_m, s.pose.position_m, &onEnd, &rv);
            double len = math_tools::Length(vec);
            math_tools::normalize(vec);
            double currValue =  dir.dot(vec);


            if (currValue>0)
            {
                //minL = len;

                if(rv < 0.0) rv = 0.0;
                if(rv > 1.0) rv = 1.0;

                minIdx = it + rv;
                newState = math_tools::interpolate(rv,A,B);

                break;
            }



            /* if (len < minL)
               {
                 minL = len;

                 if(rv < 0.0) rv = 0.0;
                 if(rv > 1.0) rv = 1.0;

                 minIdx = it + rv;
                 newState = math_tools::interpolate(rv,A,B);
               }
            */
        }
    }

    if(atEnd != NULL)
    {
        if(minIdx == (ts-1))
        {
            *atEnd = true;
        }
        else
        {
            *atEnd = false;
        }
    }
    if(closestIdx != NULL)
    {
        *closestIdx = minIdx;
    }
    return newState;
}

State Trajectory::lookAheadMaxAngle(double idx, double dist, double maxAngleRad, bool * atEnd )
{
    int next_idx = std::ceil(idx);
    if(next_idx == idx)
        next_idx++;

    if(next_idx > (int)t.size()-1)
    {
        if(atEnd)
            *atEnd = true;
        return *(t.rbegin());
    }

    State next = t[next_idx];

    State curr = this->stateAt(idx,false);
    State retState = curr;

    double dist_remaining = dist;
    bool bAtEnd = false;



    Vector3D prevVector = next.pose.position_m - curr.pose.position_m;
    math_tools::normalize(prevVector);

    while(!bAtEnd && dist_remaining > 0)
    {
        Vector3D currVector = next.pose.position_m - curr.pose.position_m;
        double dist_on_seg = math_tools::normalize(currVector);
        double currValue = fabs(acos(std::min(1.0,currVector.dot(prevVector))));
        prevVector = currVector;

        if(currValue >=maxAngleRad)
        {
            ROS_ERROR_STREAM("hitting the max angle case!");
            retState = t[std::max(0,next_idx-1)];
            break;
        }
        if(dist_on_seg > dist_remaining)
        {
            double prog = dist_remaining / dist_on_seg;
            retState = math_tools::interpolate_along_trajectory(prog, curr, next);
            dist_remaining = 0;
        }
        else
        {

            if(next_idx == (int)t.size()-1)
            {
                bAtEnd = true;
                retState = *(t.rbegin());
            }
            else
            {
                curr = next;
                next_idx++;
                dist_remaining -= dist_on_seg;
                next = t[next_idx];
            }
        }
    }
    if(atEnd)
        *atEnd = bAtEnd;
    return retState;

}

State Trajectory::lookAhead(double idx, double dist, bool * atEnd)
{
    int next_idx = std::ceil(idx);
    if(next_idx == idx)
        next_idx++;

    if(next_idx > (int)t.size()-1)
    {
        if(atEnd)
            *atEnd = true;
        return *(t.rbegin());
    }

    State next = t[next_idx];

    State curr = this->stateAt(idx,false);
    State retState = curr;

    double dist_remaining = dist;
    bool bAtEnd = false;


    while(!bAtEnd && dist_remaining > 0)
    {
        double dist_on_seg = (next.pose.position_m - curr.pose.position_m).norm();

        if(dist_on_seg > dist_remaining)
        {
            double prog = dist_remaining / dist_on_seg;
            retState = math_tools::interpolate(prog, curr, next);
            dist_remaining = 0;
        }
        else
        {
            if(next_idx == (int)t.size()-1)
            {
                bAtEnd = true;
                retState = *(t.rbegin());
            }
            else
            {
                curr = next;
                next_idx++;
                dist_remaining -= dist_on_seg;
                next = t[next_idx];
            }
        }
    }
    if(atEnd)
        *atEnd = bAtEnd;
    return retState;
}


double Trajectory::distanceToEnd(const double idx, const double maxLookAhead, const double maxAngleRad, bool *atEnd, bool *sharpCorner)
{
    if(sharpCorner)
        *sharpCorner = false;
    int next_idx = std::ceil(idx);
    if(next_idx == idx)
        next_idx++;
    next_idx = std::min(next_idx, (int)t.size()-1);
    State next = t[next_idx];

    State curr = this->stateAt(idx,false);

    double dist_remaining = maxLookAhead;
    bool bAtEnd = false;

    Vector3D prevVector = next.pose.position_m - curr.pose.position_m;
    math_tools::normalize(prevVector);
    double maxLookAheadM = maxLookAhead;

    while(!bAtEnd && dist_remaining > 0)
    {
        Vector3D currVector = next.pose.position_m - curr.pose.position_m;
        double dist_on_seg = math_tools::normalize(currVector);
        double currValue = fabs(acos(std::min(1.0,currVector.dot(prevVector))));
        prevVector = currVector;
        if(currValue > maxAngleRad)
        {
            if(sharpCorner)
                *sharpCorner = true;

            bAtEnd = true;
        }
        if(dist_on_seg > dist_remaining)
        {
            dist_remaining = 0;
        }
        else
        {
            dist_remaining -= dist_on_seg;
            if(next_idx == (int)t.size()-1 )
            {
                bAtEnd = true;
            }
            else
            {
                curr = next;
                next_idx++;
                next = t[next_idx];
            }
        }
    }
    double leng = maxLookAheadM - dist_remaining;
    if(atEnd)
        *atEnd = bAtEnd;
    return leng;

}

double Trajectory::length() const
{
    double leng = 0.0;

    if(t.size()>0)
    {
        Vector3D start = t[0].pose.position_m;
        BOOST_FOREACH(State sc, t)
        {
            leng += math_tools::Length(start,sc.pose.position_m);
            start = sc.pose.position_m;
        }
    }

    return leng;
}

size_t Trajectory::size() const
{
    return t.size();
}

size_t Plan::size() const
{
    BOOST_ASSERT( command.size() == predicted.size() );
    return command.size();
}

bool Plan::isfinite() const
{
    return command.isfinite() && predicted.isfinite();
}

Vector3D CA::msgc(const geometry_msgs::Vector3ConstPtr& msg)
{
    Vector3D initial;
    initial[0] = msg->x;
    initial[1] = msg->y;
    initial[2] = msg->z;
    return initial;
}

Vector3D CA::msgc(const geometry_msgs::Vector3& msg)
{
    Vector3D initial;
    initial[0] = msg.x;
    initial[1] = msg.y;
    initial[2] = msg.z;
    return initial;
}

/*Vector3D CA::msgc(const Vector4D &msg)
{
  Vector3D initial;
  initial[0] = msg[0];
  initial[1] = msg[1];
  initial[2] = msg[2];
  return initial;
}*/

Vector3D CA::msgc(const geometry_msgs::PointConstPtr& msg)
{
    Vector3D initial;
    initial[0] = msg->x;
    initial[1] = msg->y;
    initial[2] = msg->z;
    return initial;

}

Vector3D CA::msgc(const geometry_msgs::Point& msg)
{
    Vector3D initial;
    initial[0] = msg.x;
    initial[1] = msg.y;
    initial[2] = msg.z;
    return initial;
}

Vector3D CA::msgc(const geometry_msgs::Point32ConstPtr& msg)
{
    Vector3D initial;
    initial[0] = msg->x;
    initial[1] = msg->y;
    initial[2] = msg->z;
    return initial;

}

Vector3D CA::msgc(const geometry_msgs::Point32& msg)
{
    Vector3D initial;
    initial[0] = msg.x;
    initial[1] = msg.y;
    initial[2] = msg.z;
    return initial;
}

geometry_msgs::Vector3 CA::msgc(const Vector3D &vec)
{
    geometry_msgs::Vector3 vecr;
    vecr.x = vec[0];
    vecr.y = vec[1];
    vecr.z = vec[2];
    return vecr;
}




geometry_msgs::Point CA::msgcp(const Vector3D &vec)
{
    geometry_msgs::Point vecr;
    vecr.x = vec[0];
    vecr.y = vec[1];
    vecr.z = vec[2];
    return vecr;
}


QuatD CA::msgc(const geometry_msgs::QuaternionConstPtr& msg)
{
    return QuatD(msg->w,msg->x,msg->y,msg->z);
}
QuatD CA::msgc(const geometry_msgs::Quaternion& msg)
{
    return QuatD(msg.w,msg.x,msg.y,msg.z);
}

geometry_msgs::Quaternion CA::msgc(const QuatD &vec)
{
    geometry_msgs::Quaternion r;
    r.x = vec.x();
    r.y = vec.y();
    r.z = vec.z();
    r.w = vec.w();
    return r;
}

QuatD    CA::eulerToQuat(const Vector3D &hpr)
{

    double cr, cp, cy, sr, sp, sy, cpcy, spsy;

    // calculate trig identities
    cr = cos ( hpr[0]/2.0 );
    cp = cos ( hpr[1]/2.0 );
    cy = cos ( hpr[2]/2.0 );

    sr = sin ( hpr[0]/2.0 );
    sp = sin ( hpr[1]/2.0 );
    sy = sin ( hpr[2]/2.0 );

    cpcy = cp * cy;
    spsy = sp * sy;
    double r,i1,i2,i3;
    r = cr * cpcy + sr * spsy;
    i1 = sr * cpcy - cr * spsy;
    i2 = cr * sp * cy + sr * cp * sy;
    i3 = cr * cp * sy - sr * sp * cy;
    return QuatD ( r,i1,i2,i3 );

}


Eigen::Quaterniond CA::qinv(Eigen::Quaterniond &q)
{
    Eigen::Quaterniond q_inv;
    q_inv.x() = -q.x();
    q_inv.y() = -q.y();
    q_inv.z() = -q.z();
    q_inv.w() = q.w();
    return q_inv;
}

Vector3D CA::quatToEuler(const QuatD &quat)
{
    Vector3D hpr;
    double matrix[3][3];
    double cx,sx;
    double cy,sy;
    double cz,sz;


    matrix[0][0] = 1.0 - ( 2.0 * quat.y() * quat.y() )- ( 2.0 * quat.z() * quat.z() );
    //matrix[0][1] = (2.0 * quat->x * quat->y) - (2.0 * quat->w * quat->z);
    //matrix[0][2] = (2.0 * quat->x * quat->z) + (2.0 * quat->w * quat->y);

    matrix[1][0] = ( 2.0 * quat.x() * quat.y() ) + ( 2.0 * quat.w() * quat.z() );
    //matrix[1][1] = 1.0 - (2.0 * quat->x * quat->x)
    //                      - (2.0 * quat->z * quat->z);
    //matrix[1][2] = (2.0 * quat->y * quat->z) - (2.0 * quat->w * quat->x);

    matrix[2][0] = ( 2.0 * quat.x() * quat.z() ) - ( 2.0 * quat.w() * quat.y() );
    matrix[2][1] = ( 2.0 * quat.y() * quat.z() ) + ( 2.0 * quat.w() * quat.x() );
    matrix[2][2] = 1.0 - ( 2.0 * quat.x() * quat.x() ) - ( 2.0 * quat.y() * quat.y() );

    sy = -matrix[2][0];
    cy = sqrt ( 1.0 - ( sy * sy ) );

    hpr[1] = atan2 ( sy, cy ) ;


    if ( sy != 1.0 && sy != -1.0 )
    {
        cx = matrix[2][2] / cy;
        sx = matrix[2][1] / cy;
        hpr[0] = atan2 ( sx, cx ) ;

        cz = matrix[0][0] / cy;
        sz = matrix[1][0] / cy;

        hpr[2] = atan2 ( sz, cz ) ;
    }
    else
    {


        matrix[1][1] = 1.0 - ( 2.0 * quat.x() * quat.x() )
                       - ( 2.0 * quat.z() * quat.z() );
        matrix[1][2] = ( 2.0 * quat.y() * quat.z() ) -
                       ( 2.0 * quat.w() * quat.x() );

        cx =  matrix[1][1];
        sx = -matrix[1][2];
        hpr[0] = atan2 ( sx, cx ) ;

        cz = 1.0 ;
        sz = 0.0 ;

        hpr[2] = atan2 ( sz, cz ) ;
    }


    return hpr;
}


nav_msgs::Odometry CA::msgc(const CA::State  &state)
{
    nav_msgs::Odometry pose;
    pose.header.stamp = ros::Time(state.time_s);
    pose.pose.pose.position = msgcp(state.pose.position_m);
    pose.twist.twist.linear = msgc(state.rates.velocity_mps);
    pose.twist.twist.angular = msgc(state.rates.rot_rad);
    pose.pose.pose.orientation = msgc(eulerToQuat(state.pose.orientation_rad));

    return pose;
}

ca_common::vehicleState CA::msgcp(const CA::State  &state)
{
    ca_common::vehicleState _vehicle;
    _vehicle.header.stamp = ros::Time(state.time_s);
    _vehicle.mainRotorSpeed = state.main_rotor_speed;
    _vehicle.tailRotorSpeed = state.tail_rotor_speed;
    return _vehicle;
}

State CA::msgc(const nav_msgs::Odometry &state)
{
    State st;
    st.time_s               = state.header.stamp.toSec();
    st.pose.position_m      = msgc(state.pose.pose.position);
    st.rates.velocity_mps   = msgc(state.twist.twist.linear);
    st.rates.rot_rad        = msgc(state.twist.twist.angular);
    st.pose.orientation_rad = quatToEuler(msgc(state.pose.pose.orientation));
    st.main_rotor_speed = 27;
    st.tail_rotor_speed = 0;
    return st;
}

State CA::msgcp(const ca_common::vehicleState &state)
{
    State st;
    st.time_s               = state.header.stamp.toSec();
    st.main_rotor_speed = state.mainRotorSpeed;
    st.tail_rotor_speed = state.tailRotorSpeed;
    return st;
}
geometry_msgs::TransformStamped CA::msgtf(const nav_msgs::Odometry &msg)
{
    geometry_msgs::TransformStamped ts;
    ts.header =msg.header;
    ts.child_frame_id = msg.child_frame_id;
    ts.transform.translation.x = msg.pose.pose.position.x;
    ts.transform.translation.y = msg.pose.pose.position.y;
    ts.transform.translation.z = msg.pose.pose.position.z;
    ts.transform.rotation = msg.pose.pose.orientation;
    return ts;
}


Transform3D CA::msgc(const geometry_msgs::Transform &msg)
{
    Transform3D t(Eigen::Quaterniond( msg.rotation.w, msg.rotation.x, msg.rotation.y, msg.rotation.z));
    t.translation() = Eigen::Vector3d(msg.translation.x, msg.translation.y, msg.translation.z);

    return t;
}


Transform3D CA::msgc(const geometry_msgs::TransformStamped &msg)
{
    return msgc(msg.transform);
}


Transform3D CA::msgc(const geometry_msgs::Pose &msg)
{
    Transform3D t(Eigen::Quaterniond( msg.orientation.w, msg.orientation.x, msg.orientation.y, msg.orientation.z));
    t.translation() = Eigen::Vector3d(msg.position.x, msg.position.y, msg.position.z);

    return t;
}


geometry_msgs::Pose CA::msgc(const Transform3D &trans)
{
    geometry_msgs::Pose msg;

    msg.position = msgcp(trans.translation());
    msg.orientation = msgc(Eigen::Quaterniond(trans.rotation()));

    return msg;
}


void Trajectory::fromMsg(const ca_common::Trajectory& msg)
{
    t.clear();
    t.reserve(msg.trajectory.size());
    cost = msg.cost;
    double time_acc = 0.0;
    BOOST_FOREACH(ca_common::TrajectoryPoint tp,msg.trajectory)
    {
        State c;
        c.pose.position_m = msgc(tp.position);
        c.pose.orientation_rad[0] = 0;
        c.pose.orientation_rad[1] = 0;
        c.pose.orientation_rad[2] = tp.heading;
        if (t.size() >= 1)
            time_acc += math_tools::Length(c.pose.position_m - t.back().pose.position_m)/std::max(1e-6, math_tools::Length(t.back().rates.velocity_mps));
        c.time_s = time_acc;
        c.rates.velocity_mps = msgc(tp.velocity);
        c.rates.rot_rad[0] =   c.rates.rot_rad[1] =   c.rates.rot_rad[2] = 0.0;
        t.push_back(c);
    }
}


bool CA::operator <(const Trajectory &a,const Trajectory &b )
{
    return a.cost < b.cost;
}


bool CA::operator<(const Plan &a, const Plan &b)
{
    return a.predicted.cost < b.predicted.cost;
}

ca_common::Trajectory Trajectory::toMsg()
{
    ca_common::Trajectory traj;
    traj.cost = cost;
    BOOST_FOREACH(State d, t)
    {
        ca_common::TrajectoryPoint tp;
        tp.position = msgcp(d.pose.position_m);
        tp.heading = d.pose.orientation_rad[2];
        tp.velocity = msgc(d.rates.velocity_mps);
        traj.trajectory.push_back(tp);
    }
    return traj;
}

std::vector<geometry_msgs::Point> Trajectory::toPointList()
{
    std::vector<geometry_msgs::Point> pointlist;
    pointlist.reserve(t.size());
    BOOST_FOREACH(State d, t)
    {
        pointlist.push_back(msgcp(d.pose.position_m));
    }
    return pointlist;
}

namespace CA
{
namespace math_tools
{

double  limit2pi(double a1)
{
    int m = ( int ) ( a1/ ( 2*M_PI ) );
    a1 = a1 - m*2*M_PI;
    if(a1>M_PI)
        a1-= 2.0*M_PI;
    else if(a1<-M_PI)
        a1+=2.0*M_PI;
    return a1;
}

double  posAng(double a)
{
    if(a < 0)
    {
        a = 2*M_PI + a;
    }
    return a;
}

double Limit ( double limit, double value )
{
    if ( value>limit )
        return limit;
    else
    {
        if ( value<-limit )
            return -limit;
        else
            return value;
    }
}

double normalize(Vector3D &dir)
{
    double l = dir.norm();
    if(l>0)
    {
        dir /= l;
    }
    return l;
}

Vector3D  Limit(Vector3D & limit,Vector3D & value)
{
    return Vector3D ( Limit ( limit[0],value[0] ),Limit ( limit[1],value[1] ),Limit ( limit[2],value[2] ) );
}

double Length(const Vector3D &vec)
{
    return vec.norm();
}

double Length(const Vector3D &from, const Vector3D &to)
{
    Vector3D diff(from-to);
    return diff.norm();
}

double dot(const Vector3D &a, const Vector3D &b)
{
    return a.dot(b);
}

Vector3D cross(Vector3D a,Vector3D b)
{

    return a.cross(b);
}

double Length4D(const Vector4D &vec)
{
    return vec.norm();
}

double Length4D(const Vector4D &from, const Vector4D &to)
{
    Vector4D diff(from-to);
    return diff.norm();
}

double dot4D(const Vector4D &a, const Vector4D &b)
{
    return a.dot(b);
}

double turnDirection ( double command, double current )
{
    command =  limit2pi(command);
    current = limit2pi(current);
    double error = command - current;
    if(error < -M_PI)
        error = 2.0 * M_PI + error;
    if(error > M_PI)
        error = -2.0 * M_PI + error;
    return error;
}

double distanceSidePlane(const Vector3D &origin, const Vector3D &to, const Vector3D &check)
{
    Vector3D vec(to - origin);
    normalize(vec);
    Vector3D vec2(check - origin);
    return dot(vec,vec2);
}

Vector3D vectorToLineSegment(const Vector3D &a,const Vector3D &b, const Vector3D &c,bool *onEndPoint,double *rv)
{
    Vector3D AB(b - a);
    Vector3D AC(c - a);
    double ABl2 = dot(AB,AB)+std::numeric_limits<double>::epsilon();
    double r = dot(AC,AB) / ABl2;
    Vector3D result;
    if(r<0)
    {
        if(onEndPoint)
            *onEndPoint = true;
        result = a - c;
        // Close to  A
    } else if(r>1)
    {
        // Closer to B
        result = b - c;
        if(onEndPoint)
            *onEndPoint =true;
    } else
    {
        Vector3D mix((1.0-r) * a + r * b);
        result = mix - c;
        if(onEndPoint)
            *onEndPoint = false;
    }
    if(rv)
        *rv = r;
    return result;
}

bool isInsideTunnel(const Vector3D &a,const Vector3D &b, const Vector3D &c, double width, double height, double *rv)
{
    Vector3D vec;
    bool onEnd;
    vec = math_tools::vectorToLineSegment(a, b, c, &onEnd, rv);
    if(math_tools::Length(vec)<0.5*width && fabs(vec[2])<0.5*height)
        return true;
    else
        return false;

}

bool isInsideTunnel(const Vector3D &a,const Vector3D &b, const Plan &plan, double width, double height, double *rv)
{
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t = plan.command.getLinear();

    BOOST_FOREACH(State sc, t)
    {
        if(!isInsideTunnel(a,b,sc.pose.position_m,width,height,rv))
            return false;
    }
    return true;
}

Eigen::Matrix3d skewSymmetric(const Vector3D &v)
{
    Eigen::Matrix3d ss;

    ss << 	0, 		-v(2), 	v(1),
           v(2),	0,		-v(0),
           -v(1),	v(0),	0;

    return ss;
}

Eigen::Matrix3d rotationMatrix(const Vector3D &v)
{
    Eigen::Matrix3d R;
    double phi = v[0], thet = v[1], psi = v[2];
    R << cos(thet)*cos(psi), sin(phi)*sin(thet)*cos(psi) - cos(phi)*sin(psi), cos(phi)*sin(thet)*cos(psi) + sin(thet)*sin(psi),
      cos(thet)*sin(psi), sin(phi)*sin(thet)*sin(psi) + cos(phi)*cos(psi), cos(phi)*sin(thet)*sin(psi) - sin(phi)*cos(psi),
      0, 0, 1;
    return R;
}



Vector3D quat2mrp(const QuatD &quat)
{
    Vector3D mrp = quat.vec() / (1+quat.w());
    return mrp;
}

double deg2mrp(double deg)
{
    return (tan(deg*M_PI/720)); // mrp  = tan((deg*pi)/(4*180))
}

QuatD errorMrpToErrorQuaternion(const Vector3D &mrp_error)
{
    double norm_mrp_error = mrp_error.norm();
    double dq0 = (1 - norm_mrp_error) / (1 + norm_mrp_error);

    QuatD q_error;
    q_error.w() = dq0;
    q_error.vec() = mrp_error * (1+dq0);
    return q_error;
}


Vector3D parentVelocity(const nav_msgs::Odometry &msg)
{
    Vector3D bodyVelocity = msgc(msg.twist.twist.linear);
    QuatD orientation = msgc(msg.pose.pose.orientation);
    Vector3D pVelocity = orientation*bodyVelocity;
    return pVelocity;
}


double  smallest_angle(double angle1, double angle2)
{
    double current = limit2pi(angle1);
    double command = limit2pi(angle2);
    double diff1 = 2.0 * M_PI + command - current;
    double diff2 = -2.0 * M_PI + command - current;
    double diffb = command - current;
    if(fabs(diff2)<fabs(diffb))
    {
        diffb = diff2;
    }
    if(fabs(diff1)<fabs(diffb))
    {
        diffb = diff1;
    }
    return diffb;
}

double Length2D(const Vector3D &pd)
{

    return sqrt(pd[0]*pd[0]+pd[1]*pd[1]);
}


double Length2D(const Vector3D &p1,const Vector3D &p2)
{
    Vector3D pd = p1-p2;
    return sqrt(pd[0]*pd[0]+pd[1]*pd[1]);
}

int cell_index(int lev, int row, int col)
{
    return (int)((std::pow(4.0,lev)-1)/3+row*std::pow(2.0,lev)+col);
}


double earthMoversDistance(std::vector<double> v1, std::vector<double> v2)
{
    // TODO huh? this isn't EMD
    if(v1.size() != v2.size() )
        return -1;
    double dist = 0,distemd = 0;
    for(unsigned int i=0; i<v1.size(); i++)
    {
        double d1 = (v1[i] - v2[i]);
        dist +=  d1;
        distemd  += fabs(dist);
    }
    return distemd;
}

double earthMoversExpDistance(std::vector<double> v1, std::vector<double> v2,double factor)
{
    if(v1.size() != v2.size() )
        return -1;
    double dist = 0,distemd = 0;
    for(unsigned int i=0; i<v1.size(); i++)
    {
        double d1 = v1[i] - v2[i];
        dist += d1;
        distemd  += (1-exp(-fabs(dist)*factor));
    }
    return distemd;
}

double sumAbsDistance(std::vector<double> v1, std::vector<double> v2)
{
    if(v1.size() != v2.size() )
        return -1;
    double dist = 0;
    for(unsigned int i=0; i<v1.size(); i++)
    {
        double d1 = fabs(v1[i] - v2[i]);
        dist +=  d1;

    }
    return dist;
}


double sumExpDistance(std::vector<double> v1, std::vector<double> v2, double factor)
{
    if(v1.size() != v2.size() )
        return -1;
    double dist = 0;
    for(unsigned int i=0; i<v1.size(); i++)
    {
        double d1 =1.0 - exp(-fabs(v1[i] - v2[i]) * factor);
        dist +=  d1;

    }
    return dist;
}

double stoppingDistance(double acceleration,double reactionTime,double speed)
{
    return speed * reactionTime + speed * speed/(2.0 * acceleration);
}


double stoppingSpeed(double acceleration,double reactionTime, double distance)
{
    double sf1 = 2 * acceleration * distance;
    double sf2 = acceleration * acceleration * reactionTime;
    return std::max(0.0,-acceleration * reactionTime + std::sqrt(sf1 + sf2));
}


State interpolate(const double rv, const State &A,const State &B)
{
    State newState;
    double rm = 1.0 - rv;
    newState.time_s               = A.time_s               * rm + B.time_s               * rv;
    newState.pose.position_m      = A.pose.position_m      * rm + B.pose.position_m      * rv;
    //\TODO this probably not correct. Need to properly interpolate orientation along here.
    for(int j=0; j<3; j++)
    {
        double orient_1 = A.pose.orientation_rad[j] < 0 ? A.pose.orientation_rad[j] + 2*M_PI : A.pose.orientation_rad[j];
        double orient_2 = B.pose.orientation_rad[j] < 0 ? B.pose.orientation_rad[j] + 2*M_PI : B.pose.orientation_rad[j];
        newState.pose.orientation_rad[j] = math_tools::limit2pi(rm * orient_1 + rv * orient_2);
    }
    newState.pose.orientation_rad = B.pose.orientation_rad;// * rm + B.pose.orientation_rad * rv;
    newState.rates.velocity_mps   = A.rates.velocity_mps   * rm + B.rates.velocity_mps   * rv;
    newState.rates.rot_rad        = A.rates.rot_rad        * rm + B.rates.rot_rad        * rv;
    newState.main_rotor_speed     = A.main_rotor_speed     * rm + B.main_rotor_speed     * rv;
    newState.tail_rotor_speed     = A.tail_rotor_speed     * rm + B.tail_rotor_speed     * rv;
    return newState;
}

State interpolate_along_trajectory(const double rv, const State &A,const State &B)
{
    State newState;
    double rm = 1.0 - rv;
    newState.time_s               = A.time_s               * rm + B.time_s               * rv;
    newState.pose.position_m      = A.pose.position_m      * rm + B.pose.position_m      * rv;
    //\TODO this probably not correct. Need to properly interpolate orientation along here.
    for(int j=0; j<3; j++)
    {
        double orient_1 = A.pose.orientation_rad[j] < 0 ? A.pose.orientation_rad[j] + 2*M_PI : A.pose.orientation_rad[j];
        double orient_2 = B.pose.orientation_rad[j] < 0 ? B.pose.orientation_rad[j] + 2*M_PI : B.pose.orientation_rad[j];
        newState.pose.orientation_rad[j] = math_tools::limit2pi(rm * orient_1 + rv * orient_2);
    }
    newState.pose.orientation_rad = B.pose.orientation_rad;// * rm + B.pose.orientation_rad * rv;
    newState.rates.velocity_mps   = A.rates.velocity_mps   * rm + B.rates.velocity_mps   * rv;
    double speed = newState.rates.velocity_mps.norm();
    Eigen::Vector3d vel_vec = B.pose.position_m - A.pose.position_m; vel_vec.normalize();
    newState.rates.velocity_mps = speed*vel_vec;
    newState.rates.rot_rad        = A.rates.rot_rad        * rm + B.rates.rot_rad        * rv;
    newState.main_rotor_speed     = A.main_rotor_speed     * rm + B.main_rotor_speed     * rv;
    newState.tail_rotor_speed     = A.tail_rotor_speed     * rm + B.tail_rotor_speed     * rv;
    return newState;
}


}




Pose operator+(const Pose&p1, const Pose&p2)
{
    Pose result;
    result.position_m      = p1.position_m      + p2.position_m;
    result.orientation_rad = p1.orientation_rad + p2.orientation_rad;
    return result;
}


Pose operator*(const double&f, const Pose&p2)
{
    Pose result;
    result.position_m      = f * p2.position_m;
    result.orientation_rad = f * p2.orientation_rad;
    return result;
}

bool operator==(const Pose&p1, const Pose&p2)
{
    bool result;
    result      = (p1.position_m[0]==p2.position_m[0])&&(p1.position_m[1]==p2.position_m[1])&&(p1.position_m[2]==p2.position_m[2])&&(p1.orientation_rad[0]==p2.orientation_rad[0])&&(p1.orientation_rad[1]==p2.orientation_rad[1])&&(p1.orientation_rad[2]==p2.orientation_rad[2]);
    return result;
}

TPose operator+(const TPose&p1, const TPose&p2)
{
    TPose result;
    result.velocity_mps = p1.velocity_mps + p2.velocity_mps;
    result.rot_rad      = p1.rot_rad      + p2.rot_rad;
    return result;
}


TPose operator*(const double&f, const TPose&p2)
{
    TPose result;
    result.velocity_mps = f * p2.velocity_mps;
    result.rot_rad      = f * p2.rot_rad;
    return result;
}

std::ostream & operator<<(std::ostream &s, const QuatD &p)
{
    s<<std::fixed<<p.w()<<" "<<p.x()<<" "<<p.y()<<" "<<p.z();
    return s;
}

std::ostream & operator<<(std::ostream &s, const Pose &p)
{
    s<<std::fixed<<p.position_m[0]<<" "<<p.position_m[1]<<" "<<p.position_m[2]<<" "<<p.orientation_rad[0]<<" "<<p.orientation_rad[1]<<" "<<p.orientation_rad[2];
    return s;
}

std::ostream & operator<<(std::ostream &s, const TPose &p)
{
    s<<std::fixed<<p.velocity_mps[0]<<" "<<p.velocity_mps[1]<<" "<<p.velocity_mps[2]<<" "<<p.rot_rad[0]<<" "<<p.rot_rad[1]<<" "<<p.rot_rad[2];
    return s;
}

std::ostream & operator<<(std::ostream &s, const State &p)
{
    s<<std::fixed<<"state: "<<p.time_s<<" "<<p.pose<<" "<<p.rates<<" "<<p.main_rotor_speed<<" "<<p.tail_rotor_speed;
    return s;
}


std::ostream & operator<<(std::ostream &s, const Trajectory  &p)
{
    s<<"Trajectory-------"<<p.size()<<"\n";
    const std::vector<State, Eigen::aligned_allocator<Vector3D> > t = p.getLinear();
    BOOST_FOREACH(State sc,t)
    {
        s<<sc<<std::endl;
    }
    s<<"End--------------\n";
    return s;
}


bool isfinite(const Vector3D &v)
{
    return std::isfinite(v[0]) && std::isfinite(v[1]) && std::isfinite(v[2]);
}

bool isfinite(const Vector4D &v)
{
    return std::isfinite(v[0]) && std::isfinite(v[1]) && std::isfinite(v[2]) && std::isfinite(v[3]);
}

bool isfinite(const QuatD &v)
{
    return std::isfinite(v.x()) && std::isfinite(v.y()) && std::isfinite(v.z()) && std::isfinite(v.w());
}

bool isfinite(const Pose &p)
{
    return isfinite(p.position_m) && isfinite(p.orientation_rad);
}

bool isfinite(const TPose &p)
{
    return isfinite(p.velocity_mps) && isfinite(p.rot_rad);
}

bool isfinite(const State &s)
{
    return std::isfinite(s.time_s) && CA::isfinite(s.pose) && CA::isfinite(s.rates) && std::isfinite(s.main_rotor_speed) && std::isfinite(s.tail_rotor_speed);
}

}
