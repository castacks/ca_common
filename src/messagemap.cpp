/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ca_common/messagemap.h>
#include <string>

using namespace CA;
