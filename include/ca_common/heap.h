#ifndef __HEAP_H_
#define __HEAP_H_


//#include "customdefs.h"
//the maximum size of the heap
#define HEAPSIZE 20000000 
#define HEAPSIZE_INIT 5000
#define DSINFINITECOST 0x7fffffff

namespace CA
{

template<int key_size>
class CKey
  {
    //data
  public: 
    int key[key_size];
 
    //constructors
  public:
    CKey<key_size>(){
          for(int i = 0; i < key_size; i++)
      {	
        key[i] = 0;
      }
    };
    ~CKey<key_size>(){};
    
    //functions
  public:
    inline void SetKeytoInfinity()
    {
      for(int i = 0; i < key_size; i++)
      {	
        key[i] = DSINFINITECOST;
      }
    };
   inline void SetKeytoZero()
    {
      for(int i = 0; i < key_size; i++)
      {	
        key[i] = 0;
      }
    };
    
    
    
    //operators
  public:
    
   inline void operator = (const CKey<key_size>& RHSKey)
    {
      //iterate through the keys
      //the 0ht is the most important key
      for(int i = 0; i < key_size; i++)
        key[i] = RHSKey.key[i];
    };
    
    inline CKey<key_size> operator - (const CKey<key_size>& RHSKey) const
    {
      CKey<key_size> RetKey;
      
      if(key_size != RHSKey.key_size)
      {
        //printf("ERROR: keys should be of the same size for '-' operator\n");
        //exit(1);
      }
      
      //iterate through the keys
      //the 0ht is the most important key
      RetKey.key_size = key_size;
      for(int i = 0; i < key_size; i++)
        RetKey.key[i] = key[i] - RHSKey.key[i];
      
      return RetKey;
    };
    
    
    inline bool operator > (const CKey<key_size>& RHSKey)
    {
      //iterate through the keys
      //the 0ht is the most important key
      for(int i = 0; i < key_size; i++)
	    {
	      //compare the current key
	      if(key[i] > RHSKey.key[i])
          return true;
	      else if(key[i] < RHSKey.key[i])
          return false;
	    }
      
      //all are equal
      return false;
    };
    
    inline bool operator == (const CKey<key_size>& RHSKey)
    {
      //iterate through the keys
      //the 0ht is the most important key
      for(int i = 0; i < key_size; i++)
	    {
	      //compare the current key
	      if(key[i] != RHSKey.key[i])
          return false;
	    }
      
      //all are equal
      return true;
    };
    
    inline bool operator != (const CKey<key_size>& RHSKey)
    {
      return !(*this == RHSKey);
    };
    
    inline bool operator < (const CKey<key_size>& RHSKey)
    {	
      return (!(*this > RHSKey) && !(*this == RHSKey));
    };
    
    inline bool operator >= (const CKey<key_size>& RHSKey)
    {
      return ((*this > RHSKey) || (*this == RHSKey));
    };
    
    inline bool operator <= (const CKey<key_size>& RHSKey)
    {
      return !(*this > RHSKey);
    };
    
    inline long int operator [] (int i)
    {
      return key[i];
    };
    
  };


template <class DStarLiteNode,int key_size>
struct  heapelement
{
  DStarLiteNode *heapstate;
  CKey<key_size> key;
};

template <class DStarLiteNode,int key_size>
class CHeap
{

//data
public:
  int percolates;		//for counting purposes
   heapelement<DStarLiteNode, key_size>* heap;
  int currentsize;
  int allocated;

//constructors
public:
  CHeap();
  ~CHeap();

//functions
public:

  bool emptyheap();
  bool fullheap();
  bool inheap(DStarLiteNode *Node);
  CKey<key_size> getkeyheap(DStarLiteNode *Node);
  void makeemptyheap();
  void insertheap(DStarLiteNode *Node, CKey<key_size> key);
  void deleteheap(DStarLiteNode *Node);
  void updateheap(DStarLiteNode *Node, CKey<key_size> NewKey);
  DStarLiteNode *getminheap();
  DStarLiteNode *getminheap(CKey<key_size>& ReturnKey);
  CKey<key_size> getminkeyheap();
  DStarLiteNode *deleteminheap();
  void makeheap();

private:
  void percolatedown(int hole,  heapelement<DStarLiteNode,key_size> tmp);
  void percolateup(int hole,  heapelement<DStarLiteNode,key_size> tmp);
  void percolateupordown(int hole,  heapelement<DStarLiteNode,key_size> tmp);

  void growheap();
  void sizecheck();
  void heaperror(const char* ErrorString);
  CKey<key_size> InfiniteKey();
//operators
public:

};


}
#include <ca_common/heap.cpp>



#endif



