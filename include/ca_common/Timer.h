/**************************************************************************
 * Copyright (c) 2008, Carnegie Mellon University                         *
 *                                                                        *
 * The use of this software is governed by   *
 *                                                                        *
 * DO NOT DISTRIBUTE WITHOUT PRIOR WRITTEN PERMISSION                     *
 *                                                                        *
 * THE MATERIAL EMBODIED IN THIS SOFTWARE IS PROVIDED TO YOU "AS-IS"      *
 * AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE,       *
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR       *
 * FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL CARNEGIE MELLON   *
 * UNIVERSITY BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DIRECT,             *
 * SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY          *
 * KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,         *
 * LOSS OF PROFIT, LOSS OF USE, SAVINGS OR REVENUE, OR THE CLAIMS OF      *
 * THIRD PARTIES, WHETHER OR NOT CARNEGIE MELLON UNIVERSITY HAS BEEN      *
 * ADVISED OF THE POSSIBILITY OF SUCH LOSS, HOWEVER CAUSED AND ON         *
 * ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE      *
 * POSSESSION, USE OR PERFORMANCE OF THIS SOFTWARE.                       *
 **************************************************************************/


#ifndef __TIMER_H__
#define __TIMER_H__
#include <vector>
#include <string>
#include <ca_common/timeutil.h>
#include <stdio.h>
class ProcessTimer
{
	public:
		ProcessTimer(std::string processName)
		{
			m_processName = processName;
			gettimeofday(&m_startTime, NULL);
			gettimeofday(&m_endTime, NULL);
			m_iterations = 0;
			m_totalTime  = 0.0;
		}
		void startTime()
		{
			gettimeofday(&m_startTime, NULL);
		}
		void endTime()
		{
			gettimeofday(&m_endTime, NULL);
			m_iterations++;
			if(m_iterations > 30) 
			{
				m_iterations = 1;
				m_totalTime = 0.0;
			}
			m_totalTime += TIMEVAL_DIFF_SEC(&m_endTime, &m_startTime);			
		}
		std::string getName() { return m_processName; }
		double getFPS()
		{
			return 1.0 / (m_totalTime / (float) m_iterations);
		}
	private:
		std::string m_processName;
		struct timeval m_startTime;
		struct timeval m_endTime;
		int m_iterations;
		double m_totalTime;
};

class Timer
{
	public:

		Timer() { gettimeofday(&m_last_print_time, NULL); }
		void startTime(std::string processName)
		{
			for(unsigned int i = 0; i < m_timers.size(); i++)
			{
				if(m_timers[i]->getName() == processName)
				{
					m_timers[i]->startTime();
					return;
				}
			}
			ProcessTimer * newTimer = new ProcessTimer(processName);
			m_timers.push_back(newTimer);
		}
		void endTime(std::string processName)
		{
			for(unsigned int i = 0; i < m_timers.size(); i++)
			{
				if(m_timers[i]->getName() == processName)
				{
					m_timers[i]->endTime();
					return;
				}
			}
			ProcessTimer * newTimer = new ProcessTimer(processName);
			m_timers.push_back(newTimer);
		}
		float getFPS(std::string processName)
		{
			for(unsigned int i = 0; i < m_timers.size(); i++)
			{
				if(m_timers[i]->getName() == processName)
				{
					return m_timers[i]->getFPS();
				}
			}
		}
		void printFPS()
		{
			struct timeval curr_time;
			gettimeofday(&curr_time, NULL);
			if(TIMEVAL_DIFF_SEC(&curr_time, &m_last_print_time) < 1.0) return;

			fprintf(stderr,"-----------------------------\n");
			fprintf(stderr,"Timer: printFPS()\n");
			for(unsigned int i = 0; i < m_timers.size(); i++)
			{
				fprintf(stderr,"%s : %0.2fHz\n", m_timers[i]->getName().c_str(), m_timers[i]->getFPS());
			}
			fprintf(stderr,"-----------------------------\n");
			m_last_print_time = curr_time;
		}
		void printMilliSecs()
		{
			struct timeval curr_time;
			gettimeofday(&curr_time, NULL);
			if(TIMEVAL_DIFF_SEC(&curr_time, &m_last_print_time) < 1.0) return;

			fprintf(stderr,"-----------------------------\n");
			fprintf(stderr,"Timer: printMilliSecs()\n");
			for(unsigned int i = 0; i < m_timers.size(); i++)
			{
				fprintf(stderr,"%s : %0.2fms\n", m_timers[i]->getName().c_str(), 1000.0 / m_timers[i]->getFPS());
			}
			fprintf(stderr,"-----------------------------\n");
			m_last_print_time = curr_time;
		}
	protected:		
		std::vector<ProcessTimer *> m_timers;
		struct timeval m_last_print_time;
};

#endif

