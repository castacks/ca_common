#ifndef _SIMPLEPROFILER_H_
#define _SIMPLEPROFILER_H_

#include <string>
#include <map>
#include <ros/ros.h>

namespace CA
{
  struct ProfilerItem
  {
    double starttime;
    double sectionstarttime;
    double cummulativetime;
    double maxtime;
  };

  class Profiler
  {
  public:
    static void   start(const std::string &label);
    static void   init(const std::string &label);
    static void   sectionStart(const std::string &label);
    static void   sectionStop(const std::string &label);
    static double stop(const std::string &label,const std::vector<double> &otherdata=std::vector<double>());
    static double max(const std::string &label);
  private:
    static bool m_init;
    static std::map<std::string,CA::ProfilerItem> m_times;

    static ros::Publisher m_profilemarker;
  };

}

#endif
