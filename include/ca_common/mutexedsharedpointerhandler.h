
#include <ros/ros.h>
#include <ca_common/mutexedSharedPointer.h>
#include <ca_common/mutexedSharedPointerSrv.h>
#include <boost/thread.hpp>
#include <stdlib.h>

enum MutexedSharedPointerMode
{
   publisher,
   subscriber,
   servicePublisher,
   serviceSubscriber
};

template<class M>
class MutexedSharedPointerHandler
{
  private:
  ros::NodeHandle &n;
  ros::Publisher pub;
  ros::Subscriber sub;
  ros::ServiceServer service;
  ros::ServiceClient client;// = n.serviceClient<beginner_tutorials::AddTwoInts>("add_two_ints");
  
  std::string subscription_topic;
  std::string publish_topic;
  std::string service_topic;
  
  char *data;
  ca_common::mutexedSharedPointer msg_obj;
  M *obj_p;
  boost::mutex *mutex_p;
  MutexedSharedPointerMode mode;
      
  void setPublishTopic(std::string pub_topic);
  void subscribe(std::string topic_name);
  
  void startService(std::string servicetopic);
  void queryService();
    
  void callback(const ca_common::mutexedSharedPointer::ConstPtr& msg);
  bool serviceCallback(ca_common::mutexedSharedPointerSrv::Request &req,
                       ca_common::mutexedSharedPointerSrv::Response &res);
  void constructData(M & m_object,boost::mutex &  mu);
  void deconstructData();
  void convertDatatoMsg();
  void convertMsgtoData();
  
  public:
  
  boost::mutex* getMutex(){ return mutex_p;};
  M* getPointer(){return obj_p;};
  void publish(M & ptr2pub,boost::mutex &  mu);
  
  void initiatePublisher(std::string pub_topic){ setPublishTopic(pub_topic); mode = publisher; }
  void initiateSubscriber(std::string sub_topic){ subscribe(sub_topic); mode = subscriber; }
  void initiateServer(M & m_object,boost::mutex &  mu,std::string servicetopic)
    { 
        constructData(m_object,mu); 
        mode = servicePublisher;
        startService(servicetopic);
    }
  void initiateServiceClient(std::string servicetopic)
  {
     mode = serviceSubscriber;
     client = n.serviceClient<ca_common::mutexedSharedPointerSrv>(servicetopic);
     queryService();
  }
  MutexedSharedPointerHandler(ros::NodeHandle &n_)://TODO
  n(n_)
  {
   data = NULL;
   obj_p = NULL;
   mutex_p = NULL;
   
  };
  
  ~MutexedSharedPointerHandler()
  {
    if(data != NULL)
    delete data;
       
  };
};

template<class M>
void MutexedSharedPointerHandler<M>::setPublishTopic(std::string pub_topic)
{ 
    publish_topic = pub_topic;
    pub = n.advertise<ca_common::mutexedSharedPointer>(pub_topic, 1);
};

template<class M>
void MutexedSharedPointerHandler<M>::publish(M & ptr2pub,boost::mutex &  mu)
{
    constructData(ptr2pub,mu);
    convertDatatoMsg();
    pub.publish(msg_obj);
};

template<class M>
void MutexedSharedPointerHandler<M>::subscribe(std::string topic_name)
{ 
    subscription_topic = topic_name;
    sub  = n.subscribe(subscription_topic,1,&MutexedSharedPointerHandler<M>::callback,this);
};

template<class M>
void MutexedSharedPointerHandler<M>::startService(std::string servicetopic)
{
    service_topic = servicetopic;
    service = n.advertiseService(service_topic,&MutexedSharedPointerHandler<M>::serviceCallback,this);
};

template<class M>
void MutexedSharedPointerHandler<M>::queryService()
{
    ca_common::mutexedSharedPointerSrv srv_obj;
    srv_obj.request.request_str = "SharedPointer";
    if (client.call(srv_obj))
    {
        msg_obj = srv_obj.response.mptr;
        convertMsgtoData();
        deconstructData();
    }
    else
    {
        ROS_ERROR("Failed to call service %s",service_topic.c_str());
    return ;
    }
};


template<class M>
void MutexedSharedPointerHandler<M>::callback(const ca_common::mutexedSharedPointer::ConstPtr& msg)
{
    msg_obj= (*msg);
    convertMsgtoData();
    deconstructData();
};

template<class M>
bool MutexedSharedPointerHandler<M>::serviceCallback(ca_common::mutexedSharedPointerSrv::Request &req,
                       ca_common::mutexedSharedPointerSrv::Response &res)
{
    constructData(*obj_p,*mutex_p);
    convertDatatoMsg();
    res.mptr = msg_obj;
    
    return true;
};


template<class M>
void MutexedSharedPointerHandler<M>::constructData(M & m_object,boost::mutex & mu)
{
   if(data!=NULL)
    delete data;
   
   data = new char[sizeof(M*) + sizeof(boost::mutex*)];
   mutex_p = &mu;
   obj_p = &m_object;
   
   memcpy((void*)data,(void*)&obj_p,sizeof(M*));
   memcpy((void*)(data + sizeof(M*)),(void*)&mutex_p,sizeof(boost::mutex*));
}


template<class M>
void MutexedSharedPointerHandler<M>::deconstructData()
{
   if(data == NULL)
   {
      ROS_ERROR("MutexedSharedPointerHandler::Can't deconstruct as data is NULL");
      return;
   }
   
   obj_p =NULL;
   mutex_p =NULL;
      
   memcpy((void*)&obj_p,(void*)data,sizeof(M*));
   memcpy((void*)&mutex_p,(void*)(data + sizeof(M*)),sizeof(boost::mutex*));
   
}


template<class M>
void MutexedSharedPointerHandler<M>::convertDatatoMsg()
{ 
    if(data == NULL)
    {
        ROS_ERROR("Data is NULL, cant convert to message");
        return;
    }
    
    msg_obj.data.assign(data,data+sizeof(M*)+sizeof(boost::mutex*)); 
    
};

template<class M>
void MutexedSharedPointerHandler<M>::convertMsgtoData()
{
    if(msg_obj.data.size() != sizeof(M*)+sizeof(boost::mutex*))
    {
        ROS_ERROR_STREAM("The size of message is unexpected, either the message is corrupted or the library is wrong\nExpected Size::"<<(sizeof(M*)+sizeof(boost::mutex*))*8<<"  Message Size::"<<msg_obj.data.size());
        return;
    }
    
    if (data != NULL)
        delete data;
        
    data = new char[sizeof(M*)+ sizeof(boost::mutex*)];
        
    for(unsigned int i=0;i<msg_obj.data.size();i++)
    {
        data[i] = msg_obj.data[i];
    }    
};


