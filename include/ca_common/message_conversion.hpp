/* Copyright 2014 Sanjiban Choudhury
 * message_conversion.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef CA_COMMON_INCLUDE_CA_COMMON_MESSAGE_CONVERSION_HPP_
#define CA_COMMON_INCLUDE_CA_COMMON_MESSAGE_CONVERSION_HPP_

#include "ca_common/math.h"
#include "nav_msgs/Odometry.h"
#include "ca_nav_msgs/WorkspaceTrajectory.h"
#include "boost/foreach.hpp"

namespace ca {

namespace common_msg_conv {
CA::Trajectory ConvertWorkspaceTrajectoryMsgToTrajectory(const ca_nav_msgs::WorkspaceTrajectory &msg) {
  CA::Trajectory trajectory;
  std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > t;

  BOOST_FOREACH(const nav_msgs::Odometry &odom, msg.waypoint)
    t.push_back(CA::msgc(odom));

  trajectory.setLinear(t);
  return trajectory;
}

}
}


#endif  // CA_COMMON_INCLUDE_CA_COMMON_MESSAGE_CONVERSION_HPP_ 
