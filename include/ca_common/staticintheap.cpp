/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#ifndef _STATICINTHEAP_CPP_
#define _STATICINTHEAP_CPP_

#ifndef __STATICINTHEAP_H_
#error Only include this file from staticintheap.h
#endif

#include <deque>
#include <algorithm>

using namespace CA;

template <class DStarLiteNode,int key_size>
        StaticIntHeap<DStarLiteNode,key_size>::StaticIntHeap():
        m_maxkey(1),
        m_currmin(1),
        m_pqueue(NULL)
{
    makeemptyheap(m_maxkey);
}

template <class DStarLiteNode,int key_size>
        StaticIntHeap<DStarLiteNode,key_size>::~StaticIntHeap()
{
    if(m_pqueue)
        delete[] m_pqueue;
}

template <class DStarLiteNode,int key_size>
        void StaticIntHeap<DStarLiteNode,key_size>::makeemptyheap(unsigned int maxkey)
{
    m_maxkey = maxkey+1;
    if(m_pqueue)
        delete[] m_pqueue;
    m_pqueue = new std::deque<DStarLiteNode*> [m_maxkey];
    m_currmin = maxkey;
    currentsize = 0;
}

template <class DStarLiteNode,int key_size>
        void StaticIntHeap<DStarLiteNode,key_size>::insertheap(DStarLiteNode *o,CKey<key_size> key)
{
    unsigned int val = key.key[0];
    //assert(o->heapindex==0);
    if(val>=m_maxkey)//Skip keys larger than the size of the heap.
    {
        //std::cout<<"val>=m_maxkey: "<<val<<">="<<m_maxkey<<std::endl;
        return;
    }
    if(m_currmin>val)
        m_currmin = val;
    currentsize++;
    m_pqueue[val].push_back(o);
    //o->heapindex = val+1;

}

template <class DStarLiteNode,int key_size>
        DStarLiteNode * StaticIntHeap<DStarLiteNode,key_size>::deleteminheap()
{
    DStarLiteNode * res=NULL;
	while(m_pqueue[m_currmin].size()==0)
	{
		if(m_currmin>=(m_maxkey-1))
		{
			return NULL;
		}
		m_currmin++;
	}

	res = m_pqueue[m_currmin].front();
	m_pqueue[m_currmin].pop_front();
	currentsize--;

    return res;
}

template <class DStarLiteNode,int key_size>
        bool StaticIntHeap<DStarLiteNode,key_size>::emptyheap()
{
    //assert(1==0 &&" THIS FUNCITON IS NOT VALID ANYMORE"); TODO is this nessary
    while(m_pqueue[m_currmin].size()==0)
    {
        if(m_currmin>=(m_maxkey-1))
        {
            return true;
        }
        m_currmin++;
    }
    return false;
}

#endif

