#ifndef __STATICINTHEAP_H_
#define __STATICINTHEAP_H_

#include <ca_common/heap.h>
#include <deque>
#include <assert.h>

namespace CA
{

template <class DStarLiteNode,int key_size>
class StaticIntHeap
  {
  public:
    int currentsize;
    StaticIntHeap();
    ~StaticIntHeap();
    void makeemptyheap(unsigned int maxkey);
    inline void insertheap(DStarLiteNode *o,CKey<key_size> key);

    inline DStarLiteNode *deleteminheap();
    inline bool emptyheap();
  private:
    unsigned int m_maxkey;
    unsigned int m_currmin;
    std::deque<DStarLiteNode *>  *m_pqueue;


  };

}
#include <ca_common/staticintheap.cpp>

#endif

