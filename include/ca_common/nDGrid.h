#ifndef ND_GRID_H
#define ND_GRID_H
#include "ca_common/math.h"



namespace CA
{
	typedef Eigen::RowVectorXd RowVectorXD;
	typedef Eigen::RowVectorXi RowVectorXI;
	template <class T>
	class nDGrid
	{
		public:
		std::vector<T> _map;
		std::string _frameId;
		unsigned int _dim;
		RowVectorXD	_minValues;
		RowVectorXD	_resolution;
		RowVectorXI _numCells;
		RowVectorXD _maxValues;
		RowVectorXI _dimCellNumbers;
		unsigned int calculateCellNumbers()
		{
			for(unsigned int i=0;i<_dim;i++)
			{
				_numCells(i) = ceil((_maxValues(i)-_minValues(i))/_resolution(i));
				if(_numCells(i) == 0)
					_numCells(i) = 1;
			}
			
			_dimCellNumbers(0) = _numCells(0);
						
			for(unsigned int i=1;i<_dim;i++)
			{
				_dimCellNumbers(i) = _dimCellNumbers(i-1)*_numCells(i);
			}
			
			unsigned int totalCellNumbers = _dimCellNumbers((_dim-1));
			return totalCellNumbers;
		};
		public:
		
		RowVectorXD getMinValues()
		{
			return _minValues;
		};
		
		RowVectorXD getMaxValues()
		{
			return _maxValues;
		};
		
		RowVectorXD getResolution()
		{
			return _resolution;
		};
		
		unsigned int coord2Id(const RowVectorXD &p)
		{
			unsigned int product = 1;
			unsigned int id = std::ceil((p(0)-_minValues(0))/_resolution(0));
			for(unsigned int i=1;i<_dim;i++)
			{
				product = product*_numCells(i-1);
				unsigned int j = std::ceil((p(i)- _minValues(i))/_resolution(i));
				id = id + product*j;
			}
			return id;			
		};
		
		RowVectorXD id2Coord(unsigned int id)
		{
			RowVectorXD p;
			for(unsigned int i=_dim;i>0;i--)
			{
				p(i) = floor(id/_dimCellNumbers((i-1)))*_resolution(i) + _minValues(i) + _resolution(i)/2;
				id = id - floor(id/_dimCellNumbers((i-1)))*_dimCellNumbers((i-1));
			}
			return p;
		};	
		
		RowVectorXD getGridResolution()
		{
			return _resolution;
		};
		
		bool isInsideGrid(const RowVectorXD &p)
		{
			for(unsigned int i=0;i<_dim;i++)
			{
				if(_minValues(i) < p(i) && _maxValues(i) > p(i))
					return false;
			}
			
			return true;
		}
		
		bool isInsideGrid(const unsigned int &id)
		{
			if(id< _map.size())
				return true;
				
			return false;
		}
				
				
		T& getCellat(const RowVectorXD &p,bool& valid)
		{
			unsigned int id= 0;
			if(isInsideGrid(p))
			{
				id = coord2Id(p);
				valid = true;
				return _map[id];
				
			}
			else
			{
				ROS_ERROR_STREAM("Cannot get the cell. Point out of range");
				valid = false;
				return _map[0];
			}
		};
		
		T& getCellat(unsigned int& id,bool& valid)
		{
			if(isInsideGrid(id))
			{	
				valid = true;			
				return _map[id];
			}
			else
			{
				ROS_ERROR_STREAM("Cannot get the cell. ID out of range");
				valid = false;
				return _map[0];
			}
		};
		
		std::string getGridFrame(){ return _frameId;}
		
		bool setCellat(const RowVectorXD &p,T &t)
		{
			unsigned int id= 0;
			if(isInsideGrid(p))
			{
				id = coord2Id(p);
				_map[id] = t;
				return true;
				
			}
			else
			{
				ROS_ERROR_STREAM("Cannot set the cell. Point out of range");
				return false;
			}
		};
		
		bool setCellat(const unsigned int &id,T &t)
		{
			if(isInsideGrid(id))
			{
				_map[id] = t;
				return true;
				
			}
			else
			{
				ROS_ERROR_STREAM("Cannot set the cell. ID out of range");
				return false;
			}
		};
				
		void setMinMaxGrid(const RowVectorXD &minV, const RowVectorXD &maxV)
		{
			_minValues = minV;
			_maxValues = maxV;
		};
		
		void setGridResolution(const RowVectorXD &res)
		{
			_resolution = res;
			for(unsigned int i=0;i<_dim;i++)
			{
				if(_resolution(i)==0)
					_resolution(i)=1;
			}
		};
		
		void setGridFrame(const std::string &gridFrame)
		{
			_frameId = gridFrame;
		};
		
		void initializeGrid()
		{
			unsigned int numCells =  calculateCellNumbers();
			_map.resize(numCells);
		};
		
		void initializeGrid(const RowVectorXD& minV,const RowVectorXD& maxV,const RowVectorXD& res,std::string frameId)
		{
			_dim = minV.cols();
			_frameId = frameId;
			setMinMaxGrid(minV,maxV);
			setGridResolution(res);
			initializeGrid();
		};
		
		unsigned int getDimension(){return _dim;};
		
		template <class U>
		bool compareGridProperties(nDGrid<U>*otherGrid)
		{
			if(_dim != otherGrid->getDimension()) 
				return false;
				
			RowVectorXD otherMin = otherGrid->getMinValues();
			RowVectorXD otherMax = otherGrid->getMaxValues();
			RowVectorXD otherResolution = otherGrid->getResolution();
			
			if(_minValues == otherMin && _maxValues == otherMax && _resolution == otherResolution)
		 	{
		 		return true;
		 	}
		 	
		 	return false;
		}
		
		template <class U>
        void copyGridProperties(nDGrid<U> * otherGrid)
        {	
        	_dim = otherGrid->cols();
        	RowVectorXD otherMin = otherGrid->getMinValues();
					RowVectorXD otherMax = otherGrid->getMaxValues();
					RowVectorXD otherResolution = otherGrid->getResolution();
					initializeGrid(otherMin,otherMax,otherResolution,otherGrid->getGridFrame());			
        }
		
		nDGrid(const RowVectorXD minV,const RowVectorXD maxV,const RowVectorXD res,std::string frameId)
		{
			initializeGrid(minV,maxV,res,frameId);
		};
		
		nDGrid(){};
		
		~nDGrid()
		{
			_map.clear();
		};
		
	};
}	
#endif
