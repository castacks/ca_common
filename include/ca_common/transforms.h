/*
 * transforms.h
 *
 *  Created on: Jul 8, 2013
 *      Author: achamber
 */

#ifndef SRC_CA_COMMON_CA_COMMON_INCLUDE_CA_COMMON_TRANSFORMS_H_
#define SRC_CA_COMMON_CA_COMMON_INCLUDE_CA_COMMON_TRANSFORMS_H_

#include <ca_common/math.h>
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>
namespace CA
{

bool findTransformation(const tf::TransformListener* tf_listener, const std::string &src, const std::string &dst,
                        const ros::Time stamp, const ros::Duration duration, Transform3D& result);

Transform3D* findTransformation(const tf::TransformListener* tf_listener, const std::string &src, const std::string &dst,
                                const ros::Time stamp, const ros::Duration duration);

}

namespace TFUtils {


bool getCoordinateTransform(tf::StampedTransform &transform, tf::TransformListener &listener, std::string fromFrame,
                            std::string toFrame, double waitTime);

void transformQuaternion(tf::Transform &transform,geometry_msgs::Quaternion& quat_in);

void transformPose(tf::Transform &transform,geometry_msgs::Pose& pose_in);

void transformPoint(tf::Transform &transform,geometry_msgs::Point& point_in);

void transformPointList(tf::Transform &transform,std::vector<geometry_msgs::Point>& point_in);

void transformVector(tf::Transform &transform,geometry_msgs::Vector3& vector_in);

void transformMarker(tf::Transform &transform, visualization_msgs::Marker &marker);

void transformTrajectory(tf::Transform &transform, ca_common::Trajectory &trajectory);

void transformTrajectory(tf::Transform &transform, CA::Trajectory &trajectory);

void transformOdom(tf::Transform &transform, nav_msgs::Odometry &odom);

void transformState(tf::Transform &transform, CA::State &state);

void transformOdom(tf::Transform &transformPose, tf::Transform &transformVelocity, nav_msgs::Odometry &odom);

void transformVector3D(tf::Transform &transform, CA::Vector3D &vec);

}
#endif  // SRC_CA_COMMON_CA_COMMON_INCLUDE_CA_COMMON_TRANSFORMS_H_
