/* Copyright 2013 Sanjiban Choudhury
 * math_utils.h
 *
 *  Created on: Nov 3, 2013
 *      Author: sanjiban
 */

#ifndef MATH_UTILS_H_
#define MATH_UTILS_H_

#include "ca_common/math.h"

/**  \brief
 * For angles - use angles package in ROS
 */

namespace CA {

#define M_2PI 2.0*M_PI

namespace math_utils {

double interpolate_angles(double first_angle, double second_angle, double frac);

namespace numeric_operations {

int Wrap(int kX, int const kLowerBound, int const kUpperBound);

double Limit ( double limit, double value );

}

namespace angular_math {
/*
 * Wrap to pi
 */
double  WrapToPi (double a1);

double TurnDirection ( double command, double current ) ;

}


namespace vector_math {

/*
 * Find the psi angle of vector
 */
double Angle(const Vector3D &vec);

/*
 * Find the 2D angle between the two vectors
 */
double Angle(const Vector3D  &from, const Vector3D &to);

/*
 * Find the norm of vector ignoring the last element
 */
double Length(const Vector3D &vec);

/*
 * Find the norm of vector ignoring the last element
 */
double Length2D(const Vector3D &vec);

/*
 * Are 2 vectors equal
 */
bool Equal2D(const Vector3D &vec1, const Vector3D &vec2);

/*
 * Cross product in 2D
 */
double Cross2D(const Vector3D &vec1, const Vector3D &vec2);

/*
 * Vector to line from candidate
 */
Vector3D VectorToLine(const Vector3D &line_start, const Vector3D &line_end, const Vector3D &candidate);

/*
 * Find the 2D intersection of two vectors.
 * If they dont intersect (parallel) or intersect backwards then false is returned.
 */
bool Intersection2D(const Vector3D &from_start, const Vector3D &from_end, const Vector3D &to_start, const Vector3D &to_end, Vector3D &intersection);

/*
 * Intersection is limited to a distance offset from the interim segment
 */
bool Intersection2D(const Vector3D &from_start, const Vector3D &from_end, const Vector3D &to_start, const Vector3D &to_end, double offset_limit, Vector3D &intersection);

/*
 * Rotate by angle of another vecotr
 */
Vector3D Rotate2DByVector(const Vector3D &to_rotate, const Vector3D &rotate_around);

double DistanceSidePlane(const Eigen::Vector3d &origin, const Eigen::Vector3d &to, const Eigen::Vector3d &check);

Eigen::Vector3d Limit(const Eigen::Vector3d & limit, const Eigen::Vector3d & value);


}  // namespace vector_math

namespace transform_math {

Vector3D GetStaticVel(const State &sc);

}

}

}

#endif /* MATH_UTILS_H_ */
