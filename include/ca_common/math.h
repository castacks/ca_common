#ifndef _TX_COMMON_MATH_H_
#define _TX_COMMON_MATH_H_

#include <cmath>
#include <climits>
#include <float.h>

#include <iostream>

#include <boost/foreach.hpp>

#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <Eigen/Dense>

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/TransformStamped.h>

#include <ca_common/Trajectory.h>
#include <ca_common/vehicleState.h>
#include <ros/ros.h>

namespace CA
{
  /**
   * Definition of the 3D vector type
   */
  typedef Eigen::Vector3d Vector3D;
  typedef Eigen::Vector4d Vector4D;
  typedef Eigen::Vector3i Vector3I;
  typedef std::vector<Eigen::VectorXd> Spline1D;
  bool isfinite(const Vector3D &v);
  bool isfinite(const Vector4D &v);

  Vector3D               msgc(const geometry_msgs::Vector3ConstPtr& msg);
  Vector3D               msgc(const geometry_msgs::Vector3& msg);
  geometry_msgs::Vector3 msgc(const Vector3D &vec);
  //Vector3D               msgc(const Vector4D &msg);
  Vector3D               msgc(const geometry_msgs::PointConstPtr& msg);
  Vector3D               msgc(const geometry_msgs::Point& msg);
  Vector3D               msgc(const geometry_msgs::Point32ConstPtr& msg);
  Vector3D               msgc(const geometry_msgs::Point32& msg);

  geometry_msgs::Point   msgcp(const Vector3D &vec);

  Eigen::Quaterniond qinv(Eigen::Quaterniond &q);

  /**
   * Representation of the orientation
   */
  typedef Eigen::Quaterniond QuatD;
  static const Vector3D ZERO_3D;
  static const QuatD ZEROROT_D;

  bool isfinite(const QuatD &v);
  QuatD                     msgc(const geometry_msgs::QuaternionConstPtr& msg);
  QuatD                     msgc(const geometry_msgs::Quaternion& msg);
  geometry_msgs::Quaternion msgc(const QuatD &vec);

  QuatD    eulerToQuat(const Vector3D &euler);
  Vector3D quatToEuler(const QuatD &euler);

  std::ostream & operator<<(std::ostream &s, const CA::QuatD &p);

  /**
   * Representation of 3D Isometric transformations (translation, rotation)
   */
  typedef Eigen::Isometry3d Transform3D;
  Transform3D                 msgc(const geometry_msgs::Transform &msg);
  Transform3D                 msgc(const geometry_msgs::TransformStamped &msg);
  Transform3D                 msgc(const geometry_msgs::Pose &msg);
  geometry_msgs::Pose         msgc(const Transform3D &trans);

  /**
   * Other conversion functions
   */
  geometry_msgs::TransformStamped msgtf(const nav_msgs::Odometry &msg);

  /**
   * Covariance conversion from boost array to Eigen matrix
   */
  template<typename T, size_t N>
  inline Eigen::MatrixXd msgc( const boost::array<T, N> & matPtr )
  {
	  size_t r,c;
	  r = c = sqrt(N);

	  Eigen::MatrixXd mat(r,c);
	  for( size_t row = 0; row < r; row++ )
		  for( size_t col = 0; col < c; col++ )
			  mat( row, col ) = matPtr[ row * c + col ];

	  return mat;
  }

  /**
   * Definition of cost values
   */
  typedef int PCV;
  /**
   * The maximum cost value
   */
#define PCV_MAX      INT_MAX
  /**
   * The minimum valid cost value
   */
#define PCV_MIN      0
  /**
   * An invalid cost
   */
#define PCV_INVALID -1

  /**
   * The correct way to add two cost values.
   * This functions checks the bounds and validity.
   */
  inline PCV addCost(const PCV &a, const PCV&b)
  {
    if((a >= (PCV_MAX-b)) || (b >= (PCV_MAX-a)) )
      return PCV_MAX;
    else if (a == PCV_INVALID || b == PCV_INVALID)
      return PCV_INVALID;
    else
      return a + b;
  }

  inline PCV boostCost(const PCV &a, const double &b)
  {
    if((a >= (PCV_MAX/b)))
      return PCV_MAX;
    else if (a == PCV_INVALID)
      return PCV_INVALID;
    else
      return a*b;
  }

  /**
   * Representation of pose
   */
  class Pose
  {
  public:
    Vector3D position_m;///< Position in meters
    Vector3D orientation_rad;///< Roll,pitch,yaw orientation
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Pose():
      position_m(0,0,0),
      orientation_rad(0,0,0)
    {}
  };

  bool isfinite(const Pose &p);

  Pose operator+(const Pose&p1, const Pose&p2);
  Pose operator*(const double&f, const Pose&p);
  std::ostream & operator<<(std::ostream &s, const Pose &p);
  bool operator== (const Pose&p1, const Pose&p2);

  /**
   * Tangent space of the pose
   */

  class TPose
  {
  public:
    Vector3D velocity_mps;///< Velocity in m/s
    Vector3D rot_rad; ///< Rotational velocity in rad/s
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    TPose():
      velocity_mps(0,0,0),
      rot_rad(0,0,0)
    {}
  };

  bool isfinite(const TPose &p);

  TPose operator+(const TPose&p1, const TPose&p2);
  TPose operator*(const double&f, const TPose&p);
  std::ostream & operator<<(std::ostream &s, const TPose &p);

  /**
   * Representation of the current state of the model.
   */
  class State
  {
  public:
    double time_s;///< Global time stamp in s
    Pose pose;///< The current pose
    TPose rates;///< The rates at this location
    double main_rotor_speed;
    double tail_rotor_speed;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    State():
      time_s(0.0),
      pose(),
      rates(),
      main_rotor_speed(0.0),
      tail_rotor_speed(0.0)
    {}
  };

  bool isfinite(const State &s);

  std::ostream & operator<<(std::ostream &s, const State  &p);
  nav_msgs::Odometry msgc(const State &state);
  ca_common::vehicleState msgcp(const State &state);
  State msgc(const nav_msgs::Odometry &state);
  State msgcp(const ca_common::vehicleState &state);

  /**
   * The domain of the problem definition
   */
  class Domain
  {
  public:
    /**
     * If infinite is true the domain is not limited to a finite region.
     */
    bool infinite;
    /**
     * lower limit of domain
     */
    State min;
    /**
     * upper limit of domain
     */
    State max;
    /**
     * State increment. If 0 the state is continuous.
     */
    State inc;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Domain():
      infinite(true),
      min(),
      max(),
      inc()
    {}
  };

  /**
   * A sequence of desired states and commands
   */
  class Trajectory
  {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Trajectory():
      cost(PCV_INVALID),
      isLinear(true),
      currentPrecision(0),
      m_timeoffset(1000000000),
      t(),
      c(),
      lifetime_(DBL_MAX)
    {}
    void fromMsg(const ca_common::Trajectory& msg);
    ca_common::Trajectory  toMsg();
    std::vector<geometry_msgs::Point> toPointList();

    PCV cost;

    /**
	 * Create a new linear trajectory by specifying a list of states
	 */
    void setLinear(std::vector<State, Eigen::aligned_allocator<Vector3D> > &tt);

    /**
	 * Create a new spline based trajectory
	 */
    void setCoefs(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset);

    /**
	 * Create a new spline based trajectory
	 */
    void setCoefs4d(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset);

    /**
	 * Create a new spline based trajectory with fixed spatial discretization
	 */
    void setCoefs(std::vector<Spline1D> &splines, std::vector<double> &times, double &timeoffset, double precision);

    /**
	 * Get the spline coefficients, this will return empty if trajectory is linear
	 */
    std::vector<Spline1D> getCoefs();

    bool islinear(){return isLinear;}

    /**
	 * Change the precision used for linearization
	 */
    void changePrecision(double precision);

    /**
	 * Get a linear approximation of the trajectory, this is exact if trajectory is linear
	 */
    const std::vector<State, Eigen::aligned_allocator<Vector3D> > & getLinear()const;

    /**
     * Get the number of indices. The number is the last *valid* index. (i<=indexCount()). Returns 1 for non-linear trajectories
     */
    int indexCount();

    /**
    * Sample trajectory around a given index with given resolution, goes half-way to next/previous index.
    * WARNING: This will permanently convert a non-linear trajectory to it's linear approximation, use uniformSample instead.
    */
    std::vector<std::pair<State,double> > sampleAt(int index, float resolution)const;

    /**
     * Get the linear state based on an index.
     */
    State stateAt(int index)const;

    /**
     * Interpolate the state based on an index. Will extrapolate with set to true
     */
    State stateAt(double index, bool extrapolate=false)const;

    /**
	* Interpolate the state based on an index. Will extrapolate with set to true
	*/
    State stateAtTime(double index, bool extrapolate=false)const;

    /**
	* Sample trajectory at a particular time
	*/
    State sampleAtTime(double sample_time)const;

    /**
	*
	*/
    Trajectory segmentToTime(double index)const;

    /**
	*
	*/
    Trajectory segmentFromTime(double index)const;
     /**
     * Find closest point on trajectory to current state
     */
    State projectOnTrajInterp(State s, bool * atEnd = NULL, double * closestIdx = NULL);
    /**
	* Find intersecting point on the perpendicular of the start to end of trajectory
	*/
    State projectOnTrajInterpPerpendicular(State s, bool * atEnd = NULL, double * closestIdx = NULL);

    State lookAhead(double idx, double dist, bool * atEnd = NULL);
    State lookAheadMaxAngle(double idx, double dist, double maxAngleRad, bool * atEnd = NULL);
    double distanceToEnd(const double idx, const double maxLookAhead, const double maxAngleRad, bool * atEnd = NULL,bool * sharpCorner =NULL);
    /**
     * Returns size of linearized trajectory
     **/
    size_t size() const;

    /**
     * Length of the trajectory.
     **/
    double length() const;

    bool isfinite() const;

    double lifetime() {return lifetime_;}

    void set_lifetime(double lifetime) {lifetime_ = lifetime;}

  protected:
    bool isLinear; //is the trajectory a list of states or a spline
    double currentPrecision;
    double m_timeoffset;
    std::vector<double> segtimes;
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t; ///< Raw list of states
    std::vector<Spline1D> c; //Raw list of splines, first 4 reserved for x,y,z,heading
    double lifetime_;
    // Set t based on a sampling of the spline
    void spline2t();
    void spline2t4d();
    void spline2t(double precision);

  };
  std::ostream & operator<<(std::ostream &s, const Trajectory  &p);

  /**
   * Compare two trajectories. Returns true if a.cost < b.cost
   */
  bool operator <(const Trajectory &a,const Trajectory &b );

  class Plan
  {
  public:
    Plan():
    command(),
    predicted()
    {}

    Trajectory command;
    Trajectory predicted;

    size_t size(void) const;
    bool isfinite() const;
  };

  bool operator <(const Plan &a,const Plan &b );

  /**
   * Some math utility functions
   */
  namespace math_tools
  {

    double  limit2pi(double a1);

    double  posAng(double a);

    double Limit ( double limit, double value );

    double normalize(Vector3D &dir);

    Vector3D  Limit(Vector3D & limit,Vector3D & value);

    double Length(const Vector3D &vec);

    double Length(const Vector3D &from, const Vector3D &to);

    double dot(const Vector3D &a, const Vector3D &b);

    double Length4D(const Vector4D &vec);

    double Length4D(const Vector4D &from, const Vector4D &to);

    double dot4D(const Vector4D &a, const Vector4D &b);

    double turnDirection ( double command, double current );

    double distanceSidePlane(const Vector3D &origin, const Vector3D &to, const Vector3D &check);

    Vector3D cross(Vector3D a,Vector3D b);

    Vector3D vectorToLineSegment(const Vector3D &a,const Vector3D &b, const Vector3D &c,bool *onEndPoint=NULL,double *rv=NULL);

    bool isInsideTunnel(const Vector3D &a,const Vector3D &b, const Vector3D &c, double width, double height, double *rv=NULL);
    bool isInsideTunnel(const Vector3D &a,const Vector3D &b, const Plan &plan, double width, double height, double *rv=NULL);

    Eigen::Matrix3d skewSymmetric(const Vector3D &v);
    Eigen::Matrix3d rotationMatrix(const Vector3D &v);

    Vector3D quat2mrp(const QuatD &quat);
    double deg2mrp(double deg);
    QuatD errorMrpToErrorQuaternion(const Vector3D &mrp_error);

    Vector3D parentVelocity(const nav_msgs::Odometry &msg);

    double smallest_angle(double angle1, double angle2);
    double Length2D(const Vector3D &pd);
    double Length2D(const Vector3D &p1,const Vector3D &p2);

    int cell_index(int lev, int row, int col);
    double earthMoversDistance(std::vector<double> v1, std::vector<double> v2);
    double earthMoversExpDistance(std::vector<double> v1, std::vector<double> v2,double factor);
    double sumAbsDistance(std::vector<double> v1, std::vector<double> v2);
    double sumExpDistance(std::vector<double> v1, std::vector<double> v2,double factor);

    double stoppingDistance(double acceleration,double reactionTime,double speed);
    double stoppingSpeed(double acceleration,double reactionTime, double distance);

    State interpolate(const double fraction, const State &A,const State &B);
    State interpolate_along_trajectory(const double rv, const State &A,const State &B);
  }

  namespace math_consts
  {
    extern const double SQRT2OVER2;
    extern const double DEG2RAD;
    extern const double RAD2DEG;
    extern const double KNT2MS;
    extern const double FEETPMIN2MS;
    extern const double GRAVITY;
  }

}

#endif
