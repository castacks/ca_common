/** Coordinate conversion class: UTM <-> Latitude/Longitude
 *  Author: smaeta
 */

#ifndef _COORD_CONV_H_
#define _COORD_CONV_H_

#include <vector>

namespace CA
{

    /** UTM / Latitude Langitude coordinates conversion class
        Need to set zone params before using the conversion methods:
        1) Use ROS params:
            utm_zone_number : 1 to 60
            utm_hemisphere : 'N' or 'S' (north/south)
        2) Use method CoordinateConversion::setUTMZone(int zone, char hemisphere)
        
        This class uses PROJ.4 library: http://trac.osgeo.org/proj/wiki
     */
    class CoordinateConversion 
    {
        public:
            CoordinateConversion();

            ~CoordinateConversion();

            /** Set UTM zone used by conversor
                - zone: 1 to 60
                - hemisphere: 'N' (north) or 'S' (south)
                returns: zero if provided input parameters are valid
             */
            int setUTMZone(int zone, char hemisphere);

            /** Convert UTM coordinates to Lat/Long (radians)
                returns: zero if conversion performed correctly
             */
            int UTM2LatLongRad(double utmNorth, double utmEast, 
                               double *lat, double *lon);

            /** Convert Lat/Long (radians) to UTM coordinates
                returns: zero if conversion performed correctly
             */
            int LatLongRad2UTM(double lat, double lon, 
                               double *utmNorth, double *utmEast);
            
            /** Convert Lat/Long (degrees, minutes, seconds) to UTM coordinates
                returns: zero if conversion performed correctly
             */
            int LatLongDeg2UTM(double latDeg, double latMin, double latSec,
                               double lonDeg, double lonMin, double lonSec,
                               double *utmNorth, double *utmEast);

            /** Convert UTM Array coordinates to Lat/Long (radians)
                returns: zero if conversion performed correctly
             */
            int UTM2LatLongRad(const std::vector<double> &utmNorth,
                               const std::vector<double> &utmEast,
                               std::vector<double> &lat,
                               std::vector<double> &lon);

            /** Convert Lat/Long (radians) to UTM coordinates
                returns: zero if conversion performed correctly
             */
            int LatLongRad2UTM(const std::vector<double> &lat,
                               const std::vector<double> &lon,
                               std::vector<double> &utmNorth,
                               std::vector<double> &utmEast);

            /** Update UTM zone settings by reading ROS params
             */
            void updateUTMZoneSettings(void);
        protected:
            void *internalData;
            int  utmZone;
            char utmHemisphere;

            
            /** Check UTM zone settings
                If they are not set properly then throws an exception
             */
            void checkUTMZoneSettings(void);
            
    };

}


#endif /* _COORD_CONV_H_ */
