#ifndef SCROLLING_GRID_H
#define SCROLLING_GRID_H

#include "ca_common/math.h"


namespace CA
{
	
	template <class T>
	class Grid
	{
		public:
		std::vector<T> _map;
		std::string _frameId;
		unsigned int _xCells;							
		unsigned int _yCells;
		unsigned int _zCells;
		unsigned int _xyCells;
		float _xMax;
		float _xMin;
		float _yMax;
		float _yMin;
		float _zMax;
		float _zMin;
		double _resolutionX;
		double _resolutionY;
		double _resolutionZ;
    bool _initialized;
		
		public:
    bool isInitialized()
    {
      return _initialized;
    }    
    
    Vector3I getGridSize()
    {
        Vector3I gridSize(_xCells,_yCells,_zCells);
        return gridSize;
    }

		Vector3D getMinXYZ()
		{
			Vector3D p;
			p.x() = _xMin; p.y() = _yMin; p.z() = _zMin;
			return p;
		};
		Vector3D getMaxXYZ()
		{
			Vector3D p;
			p.x() = _xMax; p.y() = _yMax; p.z() = _zMax;
			return p;
		};
		unsigned int xyz2id(Vector3D p)
		{
			unsigned int j = floor((p.y() -_yMin)/_resolutionY);
			unsigned int i = floor((p.x() - _xMin)/_resolutionX);
			unsigned int k = floor((p.z() - _zMin)/_resolutionZ);
			return (k*_xyCells + j*_xCells + i);			
		};
		
		Vector3I xyz2sub(Vector3D p)
		{
			unsigned int j = floor((p.y() -_yMin)/_resolutionY);
			unsigned int i = floor((p.x() - _xMin)/_resolutionX);
			unsigned int k = floor((p.z() - _zMin)/_resolutionZ);
			Vector3I vId(i,j,k);
			return vId;
		};
		
		Vector3D id2xyz(unsigned int id)
		{
			Vector3D p;
			p.x() = ((id%_xyCells)%_xCells)*_resolutionX + _xMin + _resolutionX/2;
			p.y() = ((id%_xyCells)/_xCells)*_resolutionY + _yMin + _resolutionY/2;
			p.z() = (id/_xyCells)*_resolutionZ + _zMin + _resolutionZ/2;
			return p;
		};
		
		unsigned int sub2id(Vector3I p)
		{
			return (unsigned int)(p.z()*_xyCells + p.y()*_xCells + p.x());
		};    

		unsigned int xyz2idScrolling(Vector3D p)
		{
			p.x() =  p.x() - (std::floor((p.x() - _xMin)/(_xCells*_resolutionX))*_xCells*_resolutionX);
			p.y() =  p.y() - (std::floor((p.y() -_yMin)/(_yCells*_resolutionY))*_yCells*_resolutionY);
			p.z() =  p.z() - (std::floor((p.z() -_zMin)/(_zCells*_resolutionZ))*_zCells*_resolutionZ);
			
			unsigned int j = floor((p.y()-_yMin)/_resolutionY);
			unsigned int i = floor((p.x() - _xMin)/_resolutionX);
			unsigned int k = floor((p.z() - _zMin)/_resolutionZ);
			return (k*_xyCells + j*_xCells + i);			
		};

		Vector3D getGridResolution()
		{
			Vector3D resolution;
			resolution.x() = _resolutionX ;
			resolution.y() = _resolutionY;
			resolution.z() = _resolutionZ;
			
			return resolution;
		};
				
		T& getCellat(Vector3D &p,bool& valid)
		{
			unsigned int id= 0;
			if(p.x() >=_xMin && p.x() <= _xMax && p.y() >=_yMin && p.y() <= _yMax && p.z() >=_zMin && p.z()<= _zMax)
			{
				id = xyz2id(p);
				valid = true;
				return _map[id];
				
			}
			else
			{
				//ROS_ERROR_STREAM("Point::"<<p.x<<"::"<<p.y<<"::"<<p.z<<" out of range.");
				valid = false;
				return _map[0];
			}
		};
		
		T& getCellat(unsigned int& id,bool& valid)
		{
			if(id<_map.size())
			{	
				valid = true;			
				return _map[id];
			}
			else
			{
				ROS_ERROR_STREAM("Index out of bounds");
				valid = false;
				return _map[0];
			}
		};
		
		T& getCellat(Vector3I &p,bool &valid)
		{
			unsigned int id= 0;
			if(p.x() >= 0 && p.x() < (int)_xCells && p.y() >= 0 && p.y() < (int)_yCells && p.z() >= 0 && p.z() < (int)_zCells)
			{
				id = sub2id(p);
				valid = true;
				return _map[id];
			}
			else
			{
				ROS_ERROR_STREAM("Subs::"<<p.x()<<"::"<<p.y()<<"::"<<p.z()<<" out of range.");
				ROS_ERROR_STREAM("NumCells::"<<_xCells<<"::"<<_yCells<<"::"<<_zCells);
				if(p.z()< _zMin || p.z() > _zMax)
				{
					ROS_ERROR_STREAM("Z FAILED::"<<(p.z()-_zMin));
				}
				valid = false;
				return _map[0];
			}
		};

		T& getCellatScrolling(Vector3D &p,bool& valid)
		{
			unsigned int id= 0;
			id = xyz2idScrolling(p);
			valid = true;
			return _map[id];
		};
		
		T& getCellatScrolling(unsigned int& id,bool& valid)
		{
			if(id<_map.size())
			{	
				valid = true;			
				return _map[id];
			}
			else
			{
				ROS_ERROR_STREAM("Index out of bounds");
				valid = false;
				return _map[0];
			}
		};
		
		bool setCellat(Vector3D &p,T &t)
		{
			unsigned int id= 0;
			if(p.x() >= _xMin && p.x() <= _xMax && p.y() >=_yMin && p.y() <= _yMax && p.z() >=_zMin && p.z() <= _zMax)
			{
				id = xyz2id(p);
				_map[id] = t;
				return true;
			}
			else
			{
				ROS_ERROR_STREAM("Point::"<<p.x()<<"::"<<p.y()<<"::"<<p.z()<<" out of range.");
				ROS_ERROR_STREAM("Min::"<<_xMin<<"::"<<_yMin<<"::"<<_zMin<<"  Max::"<<_xMax<<"::"<<_yMax<<"::"<<_zMax);
				if(p.z()< _zMin || p.z() > _zMax)
				{
					ROS_ERROR_STREAM("Z FAILED::"<<(p.z()-_zMin));
				}
				return false;
			}
		};

		bool setCellat(Vector3I &p,T &t)
		{
			unsigned int id= 0;
			if(p.x() >= 0 && p.x() < (int)_xCells && p.y() >= 0 && p.y() < (int)_yCells && p.z() >= 0 && p.z() < (int)_zCells)
			{
				id = sub2id(p);
				_map[id] = t;
				return true;
			}
			else
			{
				ROS_ERROR_STREAM("Subs::"<<p.x()<<"::"<<p.y()<<"::"<<p.z()<<" out of range.");
				ROS_ERROR_STREAM("NumCells::"<<_xCells<<"::"<<_yCells<<"::"<<_zCells);
				if(p.z()< _zMin || p.z() > _zMax)
				{
					ROS_ERROR_STREAM("Z FAILED::"<<(p.z()-_zMin));
				}
				return false;
			}
		};
		
		bool setCellatScrolling(Vector3D &p,T &t)
		{
			unsigned int id= 0;
			id = xyz2idScrolling(p);
			_map[id] = t;
			return true;
		};
		
		void setMinMaxGrid(Vector3D minXYZ, Vector3D maxXYZ)
		{
			_xMax = maxXYZ.x();
			_xMin = minXYZ.x();
			_yMax = maxXYZ.y();
			_yMin = minXYZ.y();
			_zMax = maxXYZ.z();
			_zMin = minXYZ.z();
		};
		void setGridResolution(Vector3D resolution)
		{
			_resolutionX = resolution.x();
			_resolutionY = resolution.y();
			_resolutionZ = resolution.z();
		};
		void setGridFrame(std::string gridFrame)
		{
			_frameId = gridFrame;
		};
		
    void setGridTo(T &value)
    {
      for(size_t id=0; id < _map.size(); id++)
      {
        _map[id] = value;
      }
    };

		void initializeGrid()
		{
			_xCells = std::floor((_xMax - _xMin)/_resolutionX)+1;
			if(_xCells == 0) _xCells = 1;														
			_yCells = std::floor((_yMax - _yMin)/_resolutionY)+1;
			if(_yCells == 0) _yCells = 1;							
			_zCells = std::floor((_zMax - _zMin)/_resolutionZ)+1;
			if(_zCells == 0) _zCells = 1;
			_xyCells = _xCells*_yCells;
			_map.resize((_xyCells*_zCells));
      _initialized = true;
		};
		
		void initializeGrid(Vector3D& minXYZ,Vector3D& maxXYZ,Vector3D& resolution,std::string frameId)
		{
			_frameId = frameId;
			
			_resolutionX = resolution.x();
			if(_resolutionX ==0)
				_resolutionX = 1;
			_resolutionY = resolution.y();
			if(_resolutionY ==0)
				_resolutionY = 1;
			_resolutionZ = resolution.z();
			if(_resolutionZ ==0)
				_resolutionZ = 1;
	
			_xMax = maxXYZ.x();
			_xMin = minXYZ.x();
			_yMax = maxXYZ.y();
			_yMin = minXYZ.y();
			_zMax = maxXYZ.z();
			_zMin = minXYZ.z();
	
			_xCells = std::floor((_xMax - _xMin)/_resolutionX)+1;
			if(_xCells == 0)
			{ 
				_xCells = 1; 
				_xMax = _xMin + pow(10,-6);
			}							
			_yCells = std::floor((_yMax - _yMin)/_resolutionY)+1;
			if(_yCells == 0)
			{ 
				_yCells = 1; 
				_yMax = _yMin + pow(10,-6);
			}
			_zCells = std::floor((_zMax - _zMin)/_resolutionZ)+1;
			if(_zCells == 0)
			{ 
				_zCells = 1; 
				_zMax = _zMin + pow(10,-6);
			}
			
			_xyCells = _xCells*_yCells;
			_map.resize((_xyCells*_zCells));
      _initialized = true;
		};
		
		template <class U>
		bool compareGridProperties(Grid<U>*otherGrid)
		{
			Vector3D otherMinXYZ = otherGrid->getMinXYZ();
			Vector3D otherMaxXYZ = otherGrid->getMaxXYZ();
			Vector3D otherResolution = otherGrid->getGridResolution();
			if(_xMin == otherMinXYZ.x() && _yMin == otherMinXYZ.y() && _zMin == otherMinXYZ.z()
				 && _xMax == otherMaxXYZ.x() && _yMax == otherMaxXYZ.y() && _zMax == otherMaxXYZ.z()
				 && _resolutionX == otherResolution.x() && _resolutionY == otherResolution.y() && _resolutionZ == otherResolution.z())
		 	{
		 		return true;
		 	}
		 	
		 	return false;
		}
		
		template <class U>
        void copyGridProperties(Grid<U> * otherGrid)
        {
        	Vector3D otherMinXYZ = otherGrid->getMinXYZ();
			Vector3D otherMaxXYZ = otherGrid->getMaxXYZ();
			Vector3D otherResolution = otherGrid->getGridResolution();
			
			_xMin = otherMinXYZ.x(); _yMin = otherMinXYZ.y(); _zMin = otherMinXYZ.z();
			_xMax = otherMaxXYZ.x(); _yMax = otherMaxXYZ.y(); _zMax = otherMaxXYZ.z();
			_resolutionX = otherResolution.x(); _resolutionY = otherResolution.y(); _resolutionZ = otherResolution.z();
			initializeGrid();
        }
		
		Grid(Vector3D minXYZ,Vector3D maxXYZ,Vector3D resolution,std::string frameId)
		{
			_frameId = frameId;
			
			_resolutionX = resolution.x();
			_resolutionY = resolution.y();
			_resolutionZ = resolution.z();
	
			_xMax = maxXYZ.x();
			_xMin = minXYZ.x();
			_yMax = maxXYZ.y();
			_yMin = minXYZ.y();
			_zMax = maxXYZ.z();
			_zMin = minXYZ.z();
	
			_xCells = std::floor((_xMax - _xMin)/_resolutionX) + 1;							
			_yCells = std::floor((_yMax - _yMin)/_resolutionY) + 1;
			_zCells = std::floor((_zMax - _zMin)/_resolutionZ) + 1;
			_xyCells = _xCells*_yCells;
			_map.resize((_xyCells*_zCells));
       _initialized = true;
		};
		
		Grid()
    {
       _initialized = false;
    };
		
		~Grid()
		{
			_map.clear();
		};
		
	};
}	
#endif
