/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

//Modified by Sebastian Scherer
#ifndef _HEAP_CPP_
#define _HEAP_CPP_

#ifndef __HEAP_H_
#error Only include this file from heap.h
#endif


/**
 * @file      heap.cpp
 * @author    Maxim Likhachev
 * @date      08/20/2006
 *
 * @attention Copyright (C) 2006
 * @attention Carnegie Mellon University
 * @attention Developed under contract for ____________
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement _______
 *            between Carnegie Mellon University and the Government.
 */

/**
 * Description: This file contains the implementation of CHeap class
 **/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <math.h>

using namespace std;
using namespace CA;

#include "heap.h"





//constructors and destructors
template <class DStarLiteNode,int key_size>
CHeap<DStarLiteNode,key_size >::CHeap()
{
  percolates = 0;
  currentsize = 0;
  allocated = HEAPSIZE_INIT;

  heap = new  heapelement<DStarLiteNode,key_size>[allocated];

}

template <class DStarLiteNode,int key_size>
CHeap<DStarLiteNode,key_size >::~CHeap()
{
  int i;
  for (i=1; i<=currentsize; ++i)
    heap[i].heapstate->heapindex = 0;

  delete [] heap;
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::percolatedown(int hole,  heapelement<DStarLiteNode,key_size> tmp)
{
  int child;

  if (currentsize != 0)
  {

    for (; 2*hole <= currentsize; hole = child)
	{
	  child = 2*hole;

	  if (child != currentsize && heap[child+1].key < heap[child].key)
	    ++child;
	  if (heap[child].key < tmp.key)
	    {
	      percolates += 1;
	      heap[hole] = heap[child];
	      heap[hole].heapstate->heapindex = hole;
	    }
	  else
	    break;
	}
      heap[hole] = tmp;
      heap[hole].heapstate->heapindex = hole;
   }
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::percolateup(int hole,  heapelement<DStarLiteNode,key_size> tmp)
{
  if (currentsize != 0)
    {
      for (; hole > 1 && tmp.key < heap[hole/2].key; hole /= 2)
	  {
		percolates += 1;
		heap[hole] = heap[hole/2];
		heap[hole].heapstate->heapindex = hole;
	  }
      heap[hole] = tmp;
      heap[hole].heapstate->heapindex = hole;
    }
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::percolateupordown(int hole,  heapelement<DStarLiteNode,key_size> tmp)
{
  if (currentsize != 0)
    {
      if (hole > 1 && heap[hole/2].key > tmp.key)
		percolateup(hole, tmp);
      else
		percolatedown(hole, tmp);
    }
}

template <class DStarLiteNode,int key_size>
bool CHeap<DStarLiteNode,key_size >::emptyheap()
{
  return currentsize == 0;
}

template <class DStarLiteNode,int key_size>
bool CHeap<DStarLiteNode,key_size >::fullheap()
{
  return currentsize == HEAPSIZE-1;
}

template <class DStarLiteNode,int key_size>
bool CHeap<DStarLiteNode,key_size >::inheap(DStarLiteNode *dstarlitenode)
{
  return (dstarlitenode->heapindex != 0);
}

template <class DStarLiteNode,int key_size>
CKey<key_size> CHeap<DStarLiteNode,key_size >::getkeyheap(DStarLiteNode *dstarlitenode)
{
  if (dstarlitenode->heapindex == 0)
    heaperror("GetKey: DStarLiteNode is not in heap");

  return heap[dstarlitenode->heapindex].key;
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::makeemptyheap()
{
  int i;

  for (i=1; i<=currentsize; ++i)
    heap[i].heapstate->heapindex = 0;
  currentsize = 0;
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::makeheap()
{
  int i;

  for (i = currentsize / 2; i > 0; i--)
    {
      percolatedown(i, heap[i]);
    }
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::growheap()
{
   heapelement<DStarLiteNode,key_size>* newheap;
  int i;

 // printf("growing heap size from %d ", allocated);

  allocated = 2*allocated;
  if(allocated > HEAPSIZE)
	  allocated = HEAPSIZE;

  //printf("to %d\n", allocated);

  newheap = new  heapelement<DStarLiteNode,key_size>[allocated];

  for (i=0; i<=currentsize; ++i)
    newheap[i] = heap[i];

  delete [] heap;

  heap = newheap;
}


template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::sizecheck()
{

  if (fullheap())
    heaperror("insertheap: heap is full");
  else if(currentsize == allocated-1)
  {
	growheap();
  }
}


template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::insertheap(DStarLiteNode *dstarlitenode, CKey<key_size> key)
{
   heapelement<DStarLiteNode,key_size> tmp;
  char strTemp[100];

  sizecheck();

  if (dstarlitenode->heapindex != 0)
    {
      sprintf(strTemp, "insertheap: DStarLiteNode is already in heap");
      heaperror(strTemp);
    }
  tmp.heapstate = dstarlitenode;
  tmp.key = key;
  percolateup(++currentsize, tmp);
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::deleteheap(DStarLiteNode *dstarlitenode)
{
  if (dstarlitenode->heapindex == 0)
    heaperror("deleteheap: DStarLiteNode is not in heap");
  percolateupordown(dstarlitenode->heapindex, heap[currentsize--]);
  dstarlitenode->heapindex = 0;
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::updateheap(DStarLiteNode *dstarlitenode, CKey<key_size> NewKey)
{
  if (dstarlitenode->heapindex == 0)
    heaperror("Updateheap: DStarLiteNode is not in heap");
  if (heap[dstarlitenode->heapindex].key != NewKey)
    {
      heap[dstarlitenode->heapindex].key = NewKey;
      percolateupordown(dstarlitenode->heapindex, heap[dstarlitenode->heapindex]);
    }
}

template <class DStarLiteNode,int key_size>
DStarLiteNode* CHeap<DStarLiteNode,key_size >::getminheap()
{
  if (currentsize == 0)
    heaperror("GetMinheap: heap is empty");
  return heap[1].heapstate;
}

template <class DStarLiteNode,int key_size>
DStarLiteNode* CHeap<DStarLiteNode,key_size >::getminheap(CKey<key_size>& ReturnKey)
{
  if (currentsize == 0)
    {
      heaperror("GetMinheap: heap is empty");
      ReturnKey = InfiniteKey();
    }
  ReturnKey = heap[1].key;
  return heap[1].heapstate;
}

template <class DStarLiteNode,int key_size>
CKey<key_size> CHeap<DStarLiteNode,key_size >::getminkeyheap()
{
  CKey<key_size> ReturnKey;
  if (currentsize == 0)
    return InfiniteKey();
  ReturnKey = heap[1].key;
  return ReturnKey;
}

template <class DStarLiteNode,int key_size>
DStarLiteNode* CHeap<DStarLiteNode,key_size >::deleteminheap()
{
  DStarLiteNode *dstarlitenode;

  if (currentsize == 0)
    heaperror("DeleteMin: heap is empty");

  dstarlitenode = heap[1].heapstate;
  dstarlitenode->heapindex = 0;
  percolatedown(1, heap[currentsize--]);
  return dstarlitenode;
}

template <class DStarLiteNode,int key_size>
void CHeap<DStarLiteNode,key_size >::heaperror(const char* ErrorString)
{
  //need to send a message from here somehow
	printf("%s\n", ErrorString);
	exit(1);
}

//returns an infinite key
template <class DStarLiteNode,int key_size>
CKey<key_size> CHeap<DStarLiteNode,key_size >::InfiniteKey()
{
	CKey<key_size> key;
	key.SetKeytoInfinity();
	return key;

}


#endif
