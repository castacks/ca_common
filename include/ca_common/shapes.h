#ifndef _SHAPES_H_
#define _SHAPES_H_
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include "ca_common/math.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include <vector>

namespace CA
{
   class Shape
   {
      public:
      Shape()
      {};
      virtual double inShape(Vector3D &testpt) = 0;
      virtual visualization_msgs::Marker returnShapeMarker(int id=0)=0;
      virtual void getParameters(std::vector<double> &params)=0;
      virtual ~Shape(){};
   };
   
   class ShapeVector
   {
      public:
      std::vector<Shape*> shapeVector;
      visualization_msgs::MarkerArray returnShapeMarkerArray();
      void clearShapeVector();
      ~ShapeVector();
   };
      
   
   class Cylinder: public Shape
   {
      Vector3D pt1;
      Vector3D pt2;
      double length;
      double lengthsq;
      double radius;
      double radius_sq;
      Eigen::Quaterniond q;
      std::string paramFrame;
      
      public:
      Cylinder(Vector3D &p1_,Vector3D &p2_,double &radius_,tf::TransformListener *listener_,std::string paramFrame_ = "/world");
      Cylinder(Vector3D &p1_,Vector3D &p2_,double &radius_,std::string paramFrame_ = "/world");
      void changeParams(Vector3D &p1_,Vector3D &p2_,double &radius_);
      double inShape(Vector3D &testpt);
      visualization_msgs::Marker returnShapeMarker(int id=0);
      void getParameters(std::vector<double> &params){/* Implement this TODO */};
   };
   
   class Sphere: public Shape
   {
      Vector3D center;
      double radius;
      double radius_sq;
      tf::TransformListener *listener;
      public:
      Sphere(Vector3D &center_,double &radius_,tf::TransformListener *listener_);
      void changeParams(Vector3D &center_,double &radius_);
      double inShape(Vector3D &testpt);
      void getParameters(std::vector<double> &params);
      visualization_msgs::Marker returnShapeMarker(int id=0);
   };
   
   class Sector: public Shape
   {
		Vector3D center;
		Vector3D startAngleVector;
		Vector3D endAngleVector;
		double radius;
		Eigen::Quaterniond orientation;
		double startAngle;
		double endAngle;
		std::string frame;
		std_msgs::ColorRGBA color;
		public:
		Sector(std::string frame_)
		{
			frame = frame_;
		};
		Sector(Vector3D &center_,double & Radius,double& StartAngle,double& EndAngle,Eigen::Quaterniond &Orientation, std::string Frame,std_msgs::ColorRGBA &c);
		void setColor(std_msgs::ColorRGBA &c){color = c;};
		void setCenter(double& x, double& y, double& z){center.x() = x; center.y() = y; center.z() = z;};
		void setOrientation(Eigen::Quaterniond& Orientation){orientation = Orientation;};
		void setRadius(double &Radius){ radius = Radius;};
		void setStartEndAngle(double &startangle,double &endangle)
		{
			startAngle = startangle;
			endAngle = endangle;
			startAngleVector.x() = 1.0;startAngleVector.y() = 0.0;startAngleVector.z() = 0.0;
			endAngleVector.x() = 1.0;endAngleVector.y() = 0.0;endAngleVector.z() = 0.0;
			
			Eigen::Quaterniond q;
			q.x() = 0.0;q.y() = 0.0;q.z() = sin(startAngle/2);q.w() = cos(startAngle/2);
			startAngleVector = q*startAngleVector;
			
			q.x() = 0.0;q.y() = 0.0;q.z() = sin(endAngle/2);q.w() = cos(endAngle/2);
			endAngleVector = q*endAngleVector;
		};
		void setFrame(std::string &frame_){frame = frame_;}
		double inShape(Vector3D &testpt){return 0.0;};// Just a dummy function
        visualization_msgs::Marker returnShapeMarker(int id=0);
        void getParameters(std::vector<double> &params)
        {
        	//dummy function might code later
        };
		
   };
   
   class Frustrum: public Shape
   {
      std::vector<Vector3D> vertices;
      //std::vector<Vector3D> bottom;
      std::vector<Vector3D> normals;
      std::vector<double> intercepts;
      std::vector<double> insidePlaneHalfs;
      Vector3D insidePoint;
      
      std::string paramFrame;
      unsigned int surfaceIds[6][4];
      std_msgs::ColorRGBA color;
      std::string ns;
      public:
      Frustrum();
      Frustrum(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_,std_msgs::ColorRGBA &color_,std::string paramFrame_ = "/world",std::string ns_ = "Frustrum");
      
      void setColor(std_msgs::ColorRGBA &color_){ color = color_;};
      void setFrame(std::string frameName){paramFrame = frameName;};
      void changeParams(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_);
      void changeProperties(std::vector<Vector3D>& top_,std::vector<Vector3D>& bottom_,std_msgs::ColorRGBA &color_,std::string  paramFrame_ ,std::string ns_);
      std::vector<Vector3D> getVertices(){ return vertices;}
      Vector3D getPlaneNormal(std::vector<Vector3D>& vertices_);
      void getPlaneEquation(std::vector<Vector3D>& vertices_,double& intercept,Vector3D &normal);
      double inShape(Vector3D &testpt);
      virtual visualization_msgs::Marker returnShapeMarker(int id=0);
      void getParameters(std::vector<double> &params){/* Implement this TODO */};
      virtual ~Frustrum();
   };

}

#endif


