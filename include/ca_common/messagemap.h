#ifndef _MESSAGEMAP_H_
#define _MESSAGEMAP_H_

#include <ca_common/MissionWaypoint.h>
#include <string>

namespace CA
{
  enum WP_TYPE { WP_INVALID = ca_common::MissionWaypoint::WP_INVALID, WP_TAKEOFF = ca_common::MissionWaypoint::WP_TAKEOFF, WP_VIA = ca_common::MissionWaypoint::WP_VIA, WP_LAND = ca_common::MissionWaypoint::WP_LAND, WP_GROUND = ca_common::MissionWaypoint::WP_GROUND, WP_EXPLORE = ca_common::MissionWaypoint::WP_EXPLORE, WP_VIA_PLANNER = ca_common::MissionWaypoint::WP_VIA_PLANNER};

}

#endif
