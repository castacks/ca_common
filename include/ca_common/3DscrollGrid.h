/*author: Sezal Jain (Feb 2014)*/
#ifndef _3DSCROLLGRID_H_
#define _3DSCROLLGRID_H_
#include<iostream>
#include<Eigen/Eigen>
template<typename T>
class ScrollGrid{
private:
	int n_;			//number of dimensions in grid
	int grid_size_; 	//total number of cells in the grid.
	Eigen::Vector3i zero;		//just a n dimensional vector initialized to zero
	Eigen::Vector3i dimension_;
	Eigen::Vector3i origin_; 		//This is the rolling origin of the grid
	Eigen::Vector3i scrolloffset_;	//The offset respective to initial grid location.
	bool grid_created_;
	T* grid_;		//The actual grid of type T
public:
	ScrollGrid():
		n_(1),grid_size_(1),zero(Eigen::Vector3i::Zero(n_)),dimension_(Eigen::Vector3i::Constant(n_,1)),origin_(zero),scrolloffset_(zero),grid_created_(false){}
	ScrollGrid(Eigen::Vector3i dimension):
		n_(dimension.rows()),grid_size_(1),zero(Eigen::Vector3i::Zero(n_)),dimension_(dimension),origin_(zero),scrolloffset_(zero),grid_created_(false){
		for (int i=0;i<n_;i++){grid_size_=grid_size_*dimension_[i];}}
	
	Eigen::Vector3i getDimension(){return dimension_;}
	Eigen::Vector3i getOrigin(){return origin_;}
	Eigen::Vector3i getScrolloffset(){return scrolloffset_;}
	
	void createGrid(){grid_=new T [grid_size_];grid_created_=true;}
	void clearGrid(){if(grid_created_){for (int i=0;i<grid_size_;i++)grid_[i]=T();}std::cout<<"clear grid"<<std::endl;}
	
	bool isInsideGrid(Eigen::Vector3i x ){
		for (int i=0;i<n_;i++){if(x[i]<0||x[i]>=dimension_[i]) return false;}
		return true;}
	
	T getGC(Eigen::Vector3i x){  //Get grid cell at position x in grid. If x is outside of grid it will give error and exit.
		if(!grid_created_){std::cout<<"grid not created"<<std::endl; createGrid();}
		if(isInsideGrid(x)){
			int mempos=grid2mem(x);
			return grid_[mempos];}
			T temp; return temp;}
	void setGC(Eigen::Vector3i x, T val){  //Set value at position x in grid. If x is outside of grid, the grid will be moved so as to include x in grid and the value will be set.
		x=x-scrolloffset_;
		if(!grid_created_)createGrid();
		Eigen::Vector3i offset;
		if(!isInsideGrid(x)){offset=findOffset(x);moveGrid(offset);}
		//std::cout<<"Grid moved by"<<offset.transpose()<<std::endl;}
		int mempos=grid2mem(x);
		grid_[mempos]=val;
		return;}
private:
	void clearCells(Eigen::Vector3i start,Eigen::Vector3i finish){ //TODO improve
		Eigen::Vector3i grid_pt;
		for(int i=0;i<grid_size_;i++){
			grid_pt=mem2grid(i);
			bool flag=true;
			for (int j=0;j<n_;j++) if(grid_pt[j]<start[j]||grid_pt[j]>=finish[j])flag=false;
			if(flag)grid_[i]=T();}}
	int grid2mem(Eigen::Vector3i x){
		for (int i=0;i<n_;i++){x[i]=(x[i]+origin_[i])%dimension_[i];}
		int mem=0;int product=1;
		for (int i=0;i<n_;i++){
			mem=mem+x[i]*product;
			product=product*dimension_[i];}
		return mem;
	}
	Eigen::Vector3i mem2grid(int x){
		Eigen::Vector3i v=zero;
		int product=grid_size_;
		for (int i=n_-1;i>=0;i--){
			product=product/dimension_[i];
			v[i]=(x/product+dimension_[i]-origin_[i])%dimension_[i];
			x=x%product;}
		return v;}
	Eigen::Vector3i findOffset(Eigen::Vector3i &x){ //only returns non zero value when x lies outside the grid
		Eigen::Vector3i offset=zero;
		for (int i=0;i<n_;i++){
			if(x[i]<0){offset[i]=x[i];x[i]=0;}
			if(x[i]>=dimension_[i]){offset[i]=x[i]+1-dimension_[i];x[i]=dimension_[i]-1;}}
		return offset;}
	
	bool moveGrid(Eigen::Vector3i offset ){
		scrolloffset_= scrolloffset_+offset;
		Eigen::Vector3i start=zero;Eigen::Vector3i finish=dimension_;
		//if the offset is larger than grid dimensions in any direction, clear the grid
		for (int i=0;i<n_;i++){if(abs(offset[i])>=dimension_[i]){origin_=zero;clearGrid();return true;}}
		for (int i=0;i<n_;i++){
			if(offset[i]==0)continue;
			if(offset[i]>0)finish[i]=offset[i];
			else start[i]=dimension_[i]+offset[i];}
		clearCells(start,finish);
		origin_=origin_+offset;
		for (int i=0;i<n_;i++){origin_[i]=(origin_[i]+dimension_[i])%dimension_[i];}
		return true;}
};
#endif
