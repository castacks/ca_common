/*
 * ros_wrapper.h
 *
 *  Created on: Sep 22, 2013
 *      Author: sanjiban
 */

#ifndef ROS_WRAPPER_H_
#define ROS_WRAPPER_H_

#include <ros/ros.h>
#include <string>
#include "ca_common/math.h"
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/ColorRGBA.h>
namespace ROS {

bool getParam(ros::NodeHandle &n, const std::string& key, std::string& s);
bool getParam(ros::NodeHandle &n, const std::string& key, double& d);
bool getParam(ros::NodeHandle &n, const std::string& key, float& d);
bool getParam(ros::NodeHandle &n, const std::string& key, int& i);
bool getParam(ros::NodeHandle &n, const std::string& key, unsigned int& i);
bool getParam(ros::NodeHandle &n, const std::string& key, unsigned char& i);
bool getParam(ros::NodeHandle &n, const std::string& key, bool& b);
bool getParam(ros::NodeHandle &n, const std::string& key, XmlRpc::XmlRpcValue& v);

namespace Visualization{

bool GetMarker(const CA::Trajectory &traj, visualization_msgs::Marker &marker);

}  // namespace Visualization

}  // namespace ROS


#endif /* ROS_WRAPPER_H_ */
