#ifndef _HEATMAP_
#define _HEATMAP_
#include <visualization_msgs/Marker.h>
#include <vector>
namespace CA
{
	class ColorPoint
	{
		public:
		std_msgs::ColorRGBA _color;
		float _value;
		ColorPoint(std_msgs::ColorRGBA &c,float &value)
		{
			_color =c;
			_value = value;
		};
		ColorPoint()
		{
			_color.r = 0.0; _color.g = 0.0;_color.b = 0.0;_color.a = 1.0;
			_value = 0.0;
		};
		ColorPoint(float r,float g,float b,float a,float value)
		{
			_color.r = r;_color.g = g;_color.b = b;_color.a = a;
			_value = value;
		};
	};


	class HeatMap
	{
		private:
		std::vector<ColorPoint> _colors;      

		public:
		HeatMap()  
		{  create5ColortHeatMap();  };


		void addColorPoint( std_msgs::ColorRGBA &color,float &value);
		void clearColors() { _colors.clear(); };
		void create5ColortHeatMap();
		void create3ColortHeatMap();
		std_msgs::ColorRGBA getHeatMapColor(const float &min,const float &max, float value);
		
	};
};
#endif
