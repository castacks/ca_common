#!/usr/bin/env ruby
#
# The controller which fetches status of Jenkins job every
# 5 seconds and updates the traffic lights accordingly.
#

require "serialport"
require "json"
require "net/http"

if ARGV.length != 5
  puts "error: five arguments must be provided"
  puts
  puts "Usage:"
  puts "  ./ruby_controller.rb port job1  jenkins_host user password"
  puts
  puts "Example:"
  puts "  ./ruby_controller.rb /dev/ttyACM0 project-trunk1  jenkins.company.com"
  puts "                       johndoe 123456"
  exit 1
end

# parse params
port = ARGV[0]
job_name1 = ARGV[1]
host = ARGV[2]
user = ARGV[3]
password = ARGV[4]

# params for serial port
baud_rate = 19200
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE

sp = SerialPort.new(port, baud_rate, data_bits, stop_bits, parity)

# wait for initialization
sleep(2)

# prepare HTTPS connection
http = Net::HTTP.new(host, 8080)


while true
  data = nil

  # do the request
  http.start do |http|
    req = Net::HTTP::Get.new('/api/json')
    req.basic_auth user, password

    response = http.request(req)

    if response.code == "401"
      puts "error: given credentials are not valid; server responded with 401."
      exit 1
    end

    data = response.body
  end

  # convert returned JSON data to Ruby hash
  result = JSON.parse(data)

  # find tracked job
  tracked_job1 = nil

  result['jobs'].each do |job|
    if job['name'] == job_name1
      tracked_job1 = job
    end
  end

  if tracked_job1 == nil
    puts "error: job '" + job_name1 + "' not found"
    exit 1
  end


  # resolve state
  state = '#UUrr'
  char1 ='X'
  color1 = 'o'
  char2 = ' '
  color2 = 'o'

  if tracked_job1['color'] =~ /anime/
    char1 = 'U'
    color1 = 'o'
  elsif tracked_job1['color'] == 'red'
    char1 = 'X'
    color1 = 'r'
  elsif tracked_job1['color'] == 'yellow'
    char1 = 'Y'
    color1 = 'o'
  elsif tracked_job1['color'] == 'blue'
    char1 = 'O'
    color1 = 'g'
  end

  state = '#' + char1 + char2 + color1 + color2
  puts state
  sp.write(state)
  
  sleep(30)
end

sp.close
