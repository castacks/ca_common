# DON'T USE THIS FOR NEW PROJECTS!!!!!!!!!! #

# README #

A collection of common classes and functions. 

# Summary #

## math.h ##
* Contains a typdef on Eigen::Vector3d as Vector3D
* Bunch of math functions
* Bunch of conversion functions

## math_utils.h ##
* Useful and non-standard math functions

## coordconv ##
* A latitude longitude conversion 

## others ##
* ros_wrapper - wrapper around ros parameter call for custom printf (can be templatized)
* transform - bunch of transform utls (now new package as tf_utils)
* scrolling grids, etc

# TODO #

Ideally this will be deprecated soon into smaller packages. Here is Daniel Maturana's rant on the subject:

- 3Dscrollgrid.h: yet another scrollgrid. Filename starts with a number
UGH. The formatting is a mess UGH. This is why people start to enforce the
80-character limit. Why use std::cout? Basically a copy of gridmapping,
with all its bad parts, but possibly more bugs
- coordconv.h: nice and clean, thanks silvio. Not following preferred
standards but at least it's consistent. But is it really that common? I've
needed to to touch it. Plus it depends on a third party library, proj4.
Should go in its own package
- graph.h: again, clean, but how many packages need a graph? This is just a
thin wrapper around boost::graph, that limits its functionality. while
making it somewhat easier to use. Note that boost::graph is a huge
dependency too.
- heap.h: Apparently a heap, but seems to be specific to D* lite. pretty
esoteric.
- heatmap.h : I have no idea what this does. Ever heard of comments? Also,
why would you put a semicolon after a namespace.
- math.h: some stuff I can see - maybe. I mean, the typedef's are crap
because there's no other class you could put besides Eigen:: that would
keep the code working. The msgc stuff is useful  but should go in another
file (actually, its own package). The rest is  a bunch of random planning
stuff that probably should go in its own packages. Yeah, more than one
package, unless that dubious implementantion of earth mover's distance (how
can that be an earth mover distance??) belongs in the same package as
covariance conversion or whatever. Also, why would you keep stuff like
Length2D which is just a poor version of x.norm()?
- math_utils.h: Literally all of these functions are already in eigen, but
probably better written
- messagemap.h: just one big enum in a 380-character line. who knows what
it is?
- mutexedsharedpointerhander: this seems be useful, and is pretty clean
code (except for the lack of namespaces). But I don't really know what it
does or how to use it. Hardly common.
- nDGrid.h: yet another scroll grid. the n-dimensional part probably makes
it a lot slower, but handy whenever you need 5-dimensional occupancy grid I
guess.
- profiler.h: this is okay. not sure it really needs to be in common though.
- ros_wrapper: no idea what this is for.
- scrollingGrid.h: like nDGrid.h, limited to a lowly 3D. formatting is iffy
and there's no point in  putting a semicolon after each function. It's
otherwise ok, but should it be in common?
- shapes.h: this looks okay if you get past the terrible formatting. But
should it be in common?
- staticintheap.h: more dstar lite stuff
- Timer.h: another profiler thingy. okay but not that common.
- timeutil.h: a bunch of time-related macros which could be useful. Should
they be in common?
- transforms.h: nice and clean, and seems useful.

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.